# Warning
The addon examples in this folder are supposed to be embedded into an iframe from other examples.

They won't work if accessed directly, please use [the product pages](../product) instead.
