/* eslint-disable indent */
host.defineModule('messages', {

    clear: function(id) {
        alert('clear: ' + id);
    },
    error: function(title, body, options) {
        alert('error: ' + title + ' -> ' + body);
    },
    generic: function(title, body, options) {
        alert('generic: ' + title + ' -> ' + body);
    },
    hint: function(title, body, options) {
        alert('hint: ' + title + ' -> ' + body);
    },
    info: function(title, body, options) {
        alert('info: ' + title + ' -> ' + body);
    },
    success: function(title, body, options) {
        alert('success: ' + title + ' -> ' + body);
    },
    warning: function(title, body, options) {
        alert('warning: ' + title + ' -> ' + body);
    }
});

const kvStore = {
  setItem: function (key, value) {
    console.log("Setting item", key, value);
    window.localStorage.setItem(key, value);
  },
  getItem: function (key) {
    return new Promise(function (resolve) {
      resolve(window.localStorage.getItem(key));
    });
  }
};
host.returnsPromise(kvStore.getItem);
host.defineModule('kvStore', kvStore);

host.defineModule('featureFlag', {
  getFlag: function (key, callback) {
    const flags = {
      flag1: true,
      flag2: false
    };
    callback(flags[key] || undefined);
  }
});

function Foo(foo) {
    this.foo = foo;
    return this;
}

Foo.prototype.getFoo = function (cb) {
    if (typeof cb === 'function') {
        cb(this.foo);
    }
    return this.foo;
};

host.defineModule('moduleWithClass', {
    foo: {
        constructor: Foo,
        getFoo: Foo.prototype.getFoo
    }
});

host.defineGlobals({
   request: function(options, cb) {
       setTimeout(function() {
           cb({statusCode: 200, response: options});
       }, 10);
   }
});
