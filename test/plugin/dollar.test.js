describe('dollar', () => {
  var $;  
  beforeEach(() => {
    global.window = {};
    $ = require('../../src/plugin/dollar.js').default;
  });
  
  afterEach(() => {
    delete global.window;
    global.window = undefined;
  });

  describe('onDomLoad',() => {
    it('Executes func immediately if ready state is complete', (done) => {
      global.window = {
        document: {
          readyState: "complete"
        }  
      };
      $(done);
    });

    it('Does not execute func if ready state is not complete', (done) => {
      global.window = {
        document: {
          readyState: "loading"
        }  
      };
      $(() => done(new Error('callback invoked with wrong readyState')));
      done();
    });

    it('Executes func on load event', (done) => {
      global.window = {
        document: {
          readyState: "loading"
        },
        addEventListener: function(type, listener) {
          if (type === 'load') {
            listener();
          }  
        }
      };
      $(done);
    });
  });
});