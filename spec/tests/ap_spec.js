import AP from 'plugin/ap';
import Utils from 'common/util';

describe("AP", function () {
  var instance = new AP();
  beforeEach(function(){
    instance._data.origin = Utils.locationOrigin();
    instance._data.hostOrigin = Utils.locationOrigin();
    instance._eventHandlers = {};
    instance._data.options = {
      globalOptions: {
        check_init: true,
      }
    };
  });

  it("size is exported", function() {
    expect(instance.size).toEqual(jasmine.any(Function));
  });

  it("container is exported", function() {
    expect(instance.container).toEqual(jasmine.any(Function));
  });

  describe("AP._sendInit", function() {
    it("throws if initialization message not received", function() {
      spyOn(window, 'setTimeout');
      instance._sendInit(window);
      const timeout = window.setTimeout.calls.mostRecent().args[0];
      expect(timeout).toThrow();
    });

    it("does not throw if initialization message received", function() {
      spyOn(window, 'setTimeout');
      instance._data.options.globalOptions.check_init = true;
      instance._sendInit(window);
      instance._handleInitReceived();
      const timeout = window.setTimeout.calls.mostRecent().args[0];
      expect(timeout).not.toThrow();
    });
  });

  describe("AP._registerOnUnload", function() {
    it("registers a window.onunload event", function(done) {
      function validatorFunction (e){
        expect(e.data.type).toEqual("unload");
        instance._host.removeEventListener("message", validatorFunction);
        done();
      }
      instance._host.addEventListener("message", validatorFunction, false);

      if (typeof Event === 'function') {
        window.dispatchEvent(new Event('unload'));
      } else {
        var testEvent = document.createEvent('Event');
        testEvent.initEvent('unload', false, false);
        window.dispatchEvent(testEvent);
      }
    });
  });

  describe("AP.resize", function () {

    it("resize called with null width honours height", function () {
      var spy = jasmine.createSpy("spy");
      instance._hostModules.env = {
        resize: spy
      };
      instance.resize(null, 194);
      expect(spy.calls.count()).toBe(1);
      expect(spy).toHaveBeenCalledWith('100%', 194);
    });

  });

  describe("AP._handleEvent", function(){

    it("dispatches any", function(){
      var spy = jasmine.createSpy("spy");
      instance.registerAny([spy]);
      instance._handleEvent({
        data: {
          etyp: 'abc123'
        }
      });
      expect(spy.calls.count()).toBe(1);
    });

    it("dispatches an event", function(){
      var spy = jasmine.createSpy("spy");
      instance.register({
        some: spy
      });
      instance._handleEvent({
        data: {
          etyp: 'some'
        }
      });
      expect(spy.calls.count()).toBe(1);
    });

    it("dispatches an event + _any", function(){
      var spy = jasmine.createSpy("spy");
      var anySpy = jasmine.createSpy("anyspy");
      instance.registerAny(anySpy);
      instance.register({
        some: spy
      });
      instance._handleEvent({
        data: {
          etyp: 'some'
        }
      });
      expect(spy.calls.count()).toBe(1);
      expect(anySpy.calls.count()).toBe(1);
    });

    it("dispatched _any events have event name in _context", function(){
      var spy = jasmine.createSpy("spy");
      instance.register({
        some: spy
      });
      instance._handleEvent({
        data: {
          etyp: 'some'
        }
      });
      var eventName = spy.calls.first().args[1]._context.eventName;
      expect(eventName).toBe('some');
    });

    it("catches exceptions", function(){
      var spy = jasmine.createSpy('spy').and.throwError('some error');
      spyOn(Utils, 'error');
      instance.register({
        some: spy
      });
      instance._handleEvent({
        data: {
          etyp: 'some'
        }
      });
      expect(spy).toHaveBeenCalled();
      expect(Utils.error).toHaveBeenCalled();
    });

  });

  describe("AP promises", function(){
    var instance;
    beforeEach(function(){
      instance = new AP();
      instance._data.origin = Utils.locationOrigin();
      instance._data.hostOrigin = Utils.locationOrigin();
      instance._eventHandlers = {};
      var apiData = {
        promiseModule: {
          promiseMethod: {
            args: ['arg'],
            returnsPromise: true
          }
        }
      };
      instance._setupAPI(apiData);
      instance._setupAPIWithoutRequire(apiData);
      instance._pendingCallbacks = {};
    });

    it("returns a promise", function(){
     var thePromise = instance.promiseModule.promiseMethod();
     expect(thePromise).toBeDefined();
    });
    it("fails a promise on error", function(done){
      var errorText = 'Error text';
      var thenSpy = jasmine.createSpy('thenSpy');
      var thePromise = instance.promiseModule.promiseMethod();
      thePromise.then(thenSpy).catch(function(error){
        expect(thenSpy).not.toHaveBeenCalled();
        expect(error).toEqual(errorText);
        done();
      });
      instance._handleResponse({
        data:{
          forPlugin: true,
          mid: Object.getOwnPropertyNames(instance._pendingCallbacks)[0],
          args: [errorText, undefined]
        }
      });
    });
    it("resolves promise on success", function(done){
      var resolveResp = 'Resolve text';
      instance.promiseModule.promiseMethod().then(function(success, error){
        expect(success).toEqual(resolveResp);
        expect(error).toEqual(undefined);
        done();
      });
      instance._handleResponse({
        data:{
          forPlugin: true,
          mid: Object.getOwnPropertyNames(instance._pendingCallbacks)[0],
          args: [undefined, resolveResp]
        }
      });
    });
    it("fails a promise when both success and error are undefined", function(done){
      var thenSpy = jasmine.createSpy('thenSpy');
      var thePromise = instance.promiseModule.promiseMethod();
      thePromise.then(thenSpy).catch(function(){
        expect(thenSpy).not.toHaveBeenCalled();
        done();
      });

      instance._handleResponse({
        data:{
          forPlugin: true,
          mid: Object.getOwnPropertyNames(instance._pendingCallbacks)[0],
          args: [undefined, undefined]
        }
      });
    });

    it("uses a callback with the correct arguments if user passes a callback", function(){
      var callbackSpy = jasmine.createSpy('callbackSpy');
      instance.promiseModule.promiseMethod(callbackSpy);

      instance._handleResponse({
        data:{
          forPlugin: true,
          mid: Object.getOwnPropertyNames(instance._pendingCallbacks)[0],
          args: [undefined, 'abc123']
        }
      });

      expect(callbackSpy).toHaveBeenCalledWith('abc123');
    });

  });

  describe("AP.register", function(){

    it("register is {} by default", function () {
      expect(instance._eventHandlers).toEqual({});
      instance.register();
      expect(instance._eventHandlers).toEqual({});
    });


    it("register sets event callbacks", function () {
      var registrations = {
        event_one: function noop(){}
      };
      instance.register(registrations);
      expect(instance._eventHandlers).toEqual(registrations);
    });

    it('calling register twice merges event callbacks', function () {
      var registration_one = {
        event_one: function noop(){}
      };
      var registration_two = {
        event_two: function noop(){}
      };
      instance.register(registration_one);
      instance.register(registration_two);
      expect(instance._eventHandlers).toEqual({...registration_one, ...registration_two});
    });

  });

});
