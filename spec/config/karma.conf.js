var fs = require('fs');
var path = require('path');
var DISABLE_REPORTERS = process.env["DISABLE_REPORTERS"] !== undefined;
var customLaunchers = require('./saucelabs-launchers');
var saucelabs = process.env.SAUCE_LABS || false;
var coverage = process.env.COVERAGE || false;

function getCwd() {
  var cwd = process.cwd();

  var configSuffix = "/config";
  if (cwd.substr(-1 * configSuffix.length) === configSuffix) {
    cwd = path.join(cwd, "..", "..");
  }

  return cwd;
}

module.exports = function (config) {
  var cwd = getCwd();
  var webpackConf = {
    cache: true,
    module: {
      rules: [
        {test: /(?:\/src\/.*?\.js|\/spec\/.*?\.js)$/, loader: "babel-loader?cacheDirectory"},
        {test: /(?:\/src\/.*?\.json|\/spec\/.*?\.json)$/, loader: "json-loader"}
      ]
    },
    resolve: {
      alias: {}
    },
    mode: 'development',
    optimization: {
      minimize: false
    }
  };

  function getDirs(srcpath) {
    return fs.readdirSync(srcpath).filter(function(file) {
      return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
  }

  var base = cwd + '/src';
  getDirs(base).forEach(function (root) {
    webpackConf.resolve.alias[root] = base + '/' + root;
  });

  var karmaConfig = {
    logLevel: config.LOG_INFO,
    quiet: true,
    client: {
      captureConsole: false
    },
    basePath: '../..',
    plugins: [
      'karma-jasmine',
      'karma-webpack',
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-nyan-reporter'
    ],
    preprocessors: {
      './src/**/*.js': [ 'webpack' ],
      './spec/tests/*.js': [ 'webpack' ]
    },
    webpack: webpackConf,
    webpackServer: {
      quiet: true
    },
    reporters: ['progress', 'dots'],
    coverageReporter: {
      reporters: [
      ]
    },
    frameworks: ['jasmine'],
    browsers: ['Firefox', 'Chrome'],
    files: [
      './spec/tests/*.js',
      {pattern: 'spec/assets/*', watched: false, included: false, served: true, nocache: false}
    ],
    browserNoActivityTimeout: 80000000,
    autoWatch: true,
    exclude: [
    ]
  };

  if(saucelabs) {
    karmaConfig.reporters.push('saucelabs');
    karmaConfig.plugins.push('karma-sauce-launcher');
    karmaConfig.captureTimeout = 120000;
    karmaConfig.singleRun = true;
    karmaConfig.sauceLabs = {
      testName: 'Simple XDM unit tests',
      connectOptions : {
        verbose: true
      }
    };
    karmaConfig.customLaunchers = customLaunchers;
    karmaConfig.browsers = Object.keys(customLaunchers);
    karmaConfig.concurrency = 5;
  }

  if(coverage) {
    karmaConfig.webpack.module.postLoaders.push(
      {
        test: /\/src\/.*?\.js$/,
        loader: 'istanbul-instrumenter'
      }
    );
    karmaConfig['webpackMiddleware'] = {
      noInfo: true
    };
    karmaConfig.reporters.push('coverage');
    karmaConfig.plugins.push('karma-coverage');
    karmaConfig.coverageReporter.reporters.push({type: 'html', dir: 'coverage/', subdir: '.'});
    karmaConfig.coverageReporter.reporters.push({type: 'json', dir: 'coverage/', subdir: '.'});
  }

  config.set(karmaConfig);
};
