// https://saucelabs.com/products/supported-browsers-devices
module.exports = {
    // Chrome latest
    chrome_latest_windows: {
        base: 'SauceLabs',
        browserName: 'chrome',
        platform: 'Windows 10'
    },
    chrome_latest_osx: {
        base: 'SauceLabs',
        browserName: 'chrome',
        platform: 'macOS 13'
    },

    // Edge latest
    edge_latest_windows: {
        base: 'SauceLabs',
        browserName: 'microsoftedge',
        platform: 'Windows 10'
    },
        edge_latest_osx: {
        base: 'SauceLabs',
        browserName: 'microsoftedge',
        platform: 'macOS 13'
    },

    // Firefox latest
    firefox_latest_windows: {
        base: 'SauceLabs',
        browserName: 'firefox',
        platform: 'Windows 10'
    },
    firefox_latest_osx: {
        base: 'SauceLabs',
        browserName: 'firefox',
        platform: 'macOS 13'
    },

    // Safari latest (don't specify platform for latest version)
    safari_latest_osx: {
        base: 'SauceLabs',
        browserName: 'safari'
    }
};
