(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.combined = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _connect = _interopRequireDefault(_dereq_("../host/connect"));

var _ap = _interopRequireDefault(_dereq_("../plugin/ap"));

var _util = _interopRequireDefault(_dereq_("../common/util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Combined = /*#__PURE__*/function (_Host) {
  _inherits(Combined, _Host);

  var _super = _createSuper(Combined);

  function Combined(initCheck) {
    var _this;

    _classCallCheck(this, Combined);

    _this = _super.call(this);
    _this.parentTargets = {
      _globals: {}
    };
    var plugin = new _ap.default(undefined, initCheck); // export options from plugin to host.

    Object.getOwnPropertyNames(plugin).forEach(function (prop) {
      if (['_hostModules', '_globals'].indexOf(prop) === -1 && this[prop] === undefined) {
        this[prop] = plugin[prop];
      }
    }, _assertThisInitialized(_this));
    ['registerAny', 'register'].forEach(function (prop) {
      this[prop] = Object.getPrototypeOf(plugin)[prop].bind(plugin);
    }, _assertThisInitialized(_this)); //write plugin modules to host.

    var moduleSpec = plugin._data.api;

    if (_typeof(moduleSpec) === 'object') {
      Object.getOwnPropertyNames(moduleSpec).forEach(function (moduleName) {
        var accumulator = {};
        Object.getOwnPropertyNames(moduleSpec[moduleName]).forEach(function (methodName) {
          // class proxies
          if (moduleSpec[moduleName][methodName].hasOwnProperty('constructor')) {
            accumulator[methodName] = plugin._hostModules[moduleName][methodName].prototype;
          } else {
            // all other methods
            accumulator[methodName] = plugin._hostModules[moduleName][methodName];
            accumulator[methodName]['returnsPromise'] = moduleSpec[moduleName][methodName]['returnsPromise'] || false;
          }
        }, this);

        this._xdm.defineAPIModule(accumulator, moduleName);
      }, _assertThisInitialized(_this));
    }

    _this._hostModules = plugin._hostModules;

    _this.defineGlobal = function (module) {
      this.parentTargets['_globals'] = _util.default.extend({}, this.parentTargets['_globals'], module);

      this._xdm.defineAPIModule(module);
    };

    _this.defineModule = function (moduleName, module) {
      this._xdm.defineAPIModule(module, moduleName);

      this.parentTargets[moduleName] = {};
      Object.getOwnPropertyNames(module).forEach(function (name) {
        this.parentTargets[moduleName][name] = 'parent';
      }, this);
    };

    _this.subCreate = function (extensionOptions, initCallback) {
      extensionOptions.options = extensionOptions.options || {};
      extensionOptions.options.targets = _util.default.extend({}, this.parentTargets, extensionOptions.options.targets);
      var extension = this.create(extensionOptions, initCallback);
      return extension;
    };

    return _this;
  }

  return Combined;
}(_connect.default);

var _default = Combined;
exports.default = _default;
module.exports = exports.default;

},{"../common/util":4,"../host/connect":5,"../plugin/ap":8}],2:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _combined = _interopRequireDefault(_dereq_("./combined"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = new _combined.default();

exports.default = _default;
module.exports = exports.default;

},{"./combined":1}],3:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _util = _interopRequireDefault(_dereq_("./util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var PostMessage = /*#__PURE__*/function () {
  function PostMessage(data) {
    _classCallCheck(this, PostMessage);

    var d = data || {};

    this._registerListener(d.listenOn);
  }

  _createClass(PostMessage, [{
    key: "_registerListener",
    value: function _registerListener(listenOn) {
      if (!listenOn || !listenOn.addEventListener) {
        listenOn = window;
      }

      listenOn.addEventListener("message", _util.default._bind(this, this._receiveMessage), false);
    }
  }, {
    key: "_receiveMessage",
    value: function _receiveMessage(event) {
      var handler = this._messageHandlers[event.data.type],
          extensionId = event.data.eid,
          reg;

      if (extensionId && this._registeredExtensions) {
        reg = this._registeredExtensions[extensionId];
      }

      if (!handler || !this._checkOrigin(event, reg)) {
        return false;
      }

      handler.call(this, event, reg);
    }
  }]);

  return PostMessage;
}();

var _default = PostMessage;
exports.default = _default;
module.exports = exports.default;

},{"./util":4}],4:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var LOG_PREFIX = "[Simple-XDM] ";
var nativeBind = Function.prototype.bind;
var util = {
  locationOrigin: function locationOrigin() {
    if (!window.location.origin) {
      return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    } else {
      return window.location.origin;
    }
  },
  randomString: function randomString() {
    return Math.floor(Math.random() * 1000000000).toString(16);
  },
  isString: function isString(str) {
    return typeof str === "string" || str instanceof String;
  },
  argumentsToArray: function argumentsToArray(arrayLike) {
    return Array.prototype.slice.call(arrayLike);
  },
  argumentNames: function argumentNames(fn) {
    return fn.toString().replace(/((\/\/.*$)|(\/\*[^]*?\*\/))/mg, '') // strip comments
    .replace(/[^(]+\(([^)]*)[^]+/, '$1') // get signature
    .match(/([^\s,]+)/g) || [];
  },
  hasCallback: function hasCallback(args) {
    var length = args.length;
    return length > 0 && typeof args[length - 1] === 'function';
  },
  error: function error(msg) {
    if (window.console && window.console.error) {
      var outputError = [];

      if (typeof msg === "string") {
        outputError.push(LOG_PREFIX + msg);
        outputError = outputError.concat(Array.prototype.slice.call(arguments, 1));
      } else {
        outputError.push(LOG_PREFIX);
        outputError = outputError.concat(Array.prototype.slice.call(arguments));
      }

      window.console.error.apply(null, outputError);
    }
  },
  warn: function warn(msg) {
    if (window.console) {
      console.warn(LOG_PREFIX + msg);
    }
  },
  log: function log(msg) {
    if (window.console) {
      window.console.log(LOG_PREFIX + msg);
    }
  },
  _bind: function _bind(thisp, fn) {
    if (nativeBind && fn.bind === nativeBind) {
      return fn.bind(thisp);
    }

    return function () {
      return fn.apply(thisp, arguments);
    };
  },
  throttle: function throttle(func, wait, context) {
    var previous = 0;
    return function () {
      var now = Date.now();

      if (now - previous > wait) {
        previous = now;
        func.apply(context, arguments);
      }
    };
  },
  each: function each(list, iteratee) {
    var length;
    var key;

    if (list) {
      length = list.length;

      if (length != null && typeof list !== 'function') {
        key = 0;

        while (key < length) {
          if (iteratee.call(list[key], key, list[key]) === false) {
            break;
          }

          key += 1;
        }
      } else {
        for (key in list) {
          if (list.hasOwnProperty(key)) {
            if (iteratee.call(list[key], key, list[key]) === false) {
              break;
            }
          }
        }
      }
    }
  },
  extend: function extend(dest) {
    var args = arguments;
    var srcs = [].slice.call(args, 1, args.length);
    srcs.forEach(function (source) {
      if (_typeof(source) === "object") {
        Object.getOwnPropertyNames(source).forEach(function (name) {
          dest[name] = source[name];
        });
      }
    });
    return dest;
  },
  sanitizeStructuredClone: function sanitizeStructuredClone(object) {
    var whiteList = [Boolean, String, Date, RegExp, Blob, File, FileList, ArrayBuffer];
    var blackList = [Error, Node];
    var warn = util.warn;
    var visitedObjects = [];

    function _clone(value) {
      if (typeof value === 'function') {
        warn("A function was detected and removed from the message.");
        return null;
      }

      if (blackList.some(function (t) {
        if (value instanceof t) {
          warn("".concat(t.name, " object was detected and removed from the message."));
          return true;
        }

        return false;
      })) {
        return {};
      }

      if (value && _typeof(value) === 'object' && whiteList.every(function (t) {
        return !(value instanceof t);
      })) {
        var newValue;

        if (Array.isArray(value)) {
          newValue = value.map(function (element) {
            return _clone(element);
          });
        } else {
          if (visitedObjects.indexOf(value) > -1) {
            warn("A circular reference was detected and removed from the message.");
            return null;
          }

          visitedObjects.push(value);
          newValue = {};

          for (var name in value) {
            if (value.hasOwnProperty(name)) {
              var clonedValue = _clone(value[name]);

              if (clonedValue !== null) {
                newValue[name] = clonedValue;
              }
            }
          }

          visitedObjects.pop();
        }

        return newValue;
      }

      return value;
    }

    return _clone(object);
  },
  getOrigin: function getOrigin(url, base) {
    // everything except IE11
    if (typeof URL === 'function') {
      try {
        return new URL(url, base).origin;
      } catch (e) {}
    } // ie11 + safari 10


    var doc = document.implementation.createHTMLDocument('');

    if (base) {
      var baseElement = doc.createElement('base');
      baseElement.href = base;
      doc.head.appendChild(baseElement);
    }

    var anchorElement = doc.createElement('a');
    anchorElement.href = url;
    doc.body.appendChild(anchorElement);
    var origin = anchorElement.protocol + '//' + anchorElement.hostname; //ie11, only include port if referenced in initial URL

    if (url.match(/\/\/[^/]+:[0-9]+\//)) {
      origin += anchorElement.port ? ':' + anchorElement.port : '';
    }

    return origin;
  }
};
var _default = util;
exports.default = _default;
module.exports = exports.default;

},{}],5:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _xdmrpc = _interopRequireDefault(_dereq_("./xdmrpc"));

var _util = _interopRequireDefault(_dereq_("../common/util"));

var _featureFlag = _dereq_("./feature-flag");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Connect = /*#__PURE__*/function () {
  function Connect() {
    _classCallCheck(this, Connect);

    this._xdm = new _xdmrpc.default();
  }
  /**
   * Send a message to iframes matching the targetSpec. This message is added to
   *  a message queue for delivery to ensure the message is received if an iframe
   *  has not yet loaded
   *
   * @param type The name of the event type
   * @param targetSpec The spec to match against extensions when sending this event
   * @param event The event payload
   * @param callback A callback to be executed when the remote iframe calls its callback
   */


  _createClass(Connect, [{
    key: "dispatch",
    value: function dispatch(type, targetSpec, event, callback) {
      this._xdm.queueEvent(type, targetSpec, event, callback);

      return this.getExtensions(targetSpec);
    }
    /**
     * Send a message to iframes matching the targetSpec immediately. This message will
     *  only be sent to iframes that are already open, and will not be delivered if none
     *  are currently open.
     *
     * @param type The name of the event type
     * @param targetSpec The spec to match against extensions when sending this event
     * @param event The event payload
     */

  }, {
    key: "broadcast",
    value: function broadcast(type, targetSpec, event) {
      this._xdm.dispatch(type, targetSpec, event, null, null);

      return this.getExtensions(targetSpec);
    }
  }, {
    key: "_createId",
    value: function _createId(extension) {
      if (!extension.addon_key || !extension.key) {
        throw Error('Extensions require addon_key and key');
      }

      return extension.addon_key + '__' + extension.key + '__' + _util.default.randomString();
    }
    /**
    * Creates a new iframed module, without actually creating the DOM element.
    * The iframe attributes are passed to the 'setupCallback', which is responsible for creating
    * the DOM element and returning the window reference.
    *
    * @param extension The extension definition. Example:
    *   {
    *     addon_key: 'my-addon',
    *     key: 'my-module',
    *     url: 'https://example.com/my-module',
    *     options: {
    *         autoresize: false,
    *         hostOrigin: 'https://connect-host.example.com/'
    *     }
    *   }
    *
    * @param initCallback The optional initCallback is called when the bridge between host and iframe is established.
    **/

  }, {
    key: "create",
    value: function create(extension, initCallback, unloadCallback) {
      var extension_id = this.registerExtension(extension, initCallback, unloadCallback);
      var options = extension.options || {};
      options.platformFeatureFlags = (0, _featureFlag.getPlatformFeatureFlags)();
      var data = {
        extension_id: extension_id,
        api: this._xdm.getApiSpec(extension.addon_key),
        origin: _util.default.locationOrigin(),
        options: options
      };
      return {
        id: extension_id,
        name: JSON.stringify(data),
        src: extension.url
      };
    } // This is called from ACJS
    // noinspection JSUnusedGlobalSymbols

  }, {
    key: "registerRequestNotifier",
    value: function registerRequestNotifier(callback) {
      this._xdm.registerRequestNotifier(callback);
    }
  }, {
    key: "registerExtension",
    value: function registerExtension(extension, initCallback, unloadCallback) {
      var extension_id = this._createId(extension);

      this._xdm.registerExtension(extension_id, {
        extension: extension,
        initCallback: initCallback,
        unloadCallback: unloadCallback
      });

      return extension_id;
    }
  }, {
    key: "registerKeyListener",
    value: function registerKeyListener(extension_id, key, modifiers, callback) {
      this._xdm.registerKeyListener(extension_id, key, modifiers, callback);
    }
  }, {
    key: "unregisterKeyListener",
    value: function unregisterKeyListener(extension_id, key, modifiers, callback) {
      this._xdm.unregisterKeyListener(extension_id, key, modifiers, callback);
    }
  }, {
    key: "registerClickHandler",
    value: function registerClickHandler(callback) {
      this._xdm.registerClickHandler(callback);
    }
  }, {
    key: "unregisterClickHandler",
    value: function unregisterClickHandler() {
      this._xdm.unregisterClickHandler();
    }
  }, {
    key: "defineModule",
    value: function defineModule(moduleName, module, options) {
      this._xdm.defineAPIModule(module, moduleName, options);
    }
  }, {
    key: "isModuleDefined",
    value: function isModuleDefined(moduleName) {
      return this._xdm.isAPIModuleDefined(moduleName);
    }
  }, {
    key: "defineGlobals",
    value: function defineGlobals(module) {
      this._xdm.defineAPIModule(module);
    }
  }, {
    key: "getExtensions",
    value: function getExtensions(filter) {
      return this._xdm.getRegisteredExtensions(filter);
    }
  }, {
    key: "unregisterExtension",
    value: function unregisterExtension(filter) {
      return this._xdm.unregisterExtension(filter);
    }
  }, {
    key: "returnsPromise",
    value: function returnsPromise(wrappedMethod) {
      wrappedMethod.returnsPromise = true;
    }
  }, {
    key: "setFeatureFlagGetter",
    value: function setFeatureFlagGetter(getBooleanFeatureFlag) {
      this._xdm.setFeatureFlagGetter(getBooleanFeatureFlag);
    }
  }, {
    key: "registerExistingExtension",
    value: function registerExistingExtension(extension_id, data) {
      return this._xdm.registerExtension(extension_id, data);
    }
  }]);

  return Connect;
}();

var _default = Connect;
exports.default = _default;
module.exports = exports.default;

},{"../common/util":4,"./feature-flag":6,"./xdmrpc":7}],6:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPlatformFeatureFlags = getPlatformFeatureFlags;
var allowedPlatformFeatureFlags = ['platform-visual-refresh-icons'];

function getPlatformFeatureFlags() {
  var flags = {};

  if (window.connectHost && window.connectHost.getBooleanFeatureFlag) {
    allowedPlatformFeatureFlags.forEach(function (key) {
      return flags[key] = window.connectHost.getBooleanFeatureFlag(key);
    });
  }

  return flags;
}

},{}],7:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _util = _interopRequireDefault(_dereq_("../common/util"));

var _postmessage = _interopRequireDefault(_dereq_("../common/postmessage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var VALID_EVENT_TIME_MS = 30000; //30 seconds

var XDMRPC = /*#__PURE__*/function (_PostMessage) {
  _inherits(XDMRPC, _PostMessage);

  var _super = _createSuper(XDMRPC);

  function XDMRPC(config) {
    var _this;

    _classCallCheck(this, XDMRPC);

    config = config || {};
    _this = _super.call(this, config);
    _this._registeredExtensions = config.extensions || {};
    _this._registeredAPIModules = {};
    _this._registeredAPIModules._globals = {};
    _this._pendingCallbacks = {};
    _this._keycodeCallbacks = {};
    _this._clickHandlers = [];
    _this._pendingEvents = {};
    _this._messageHandlers = {
      init: _this._handleInit,
      req: _this._handleRequest,
      resp: _this._handleResponse,
      broadcast: _this._handleBroadcast,
      event_query: _this._handleEventQuery,
      key_triggered: _this._handleKeyTriggered,
      addon_clicked: _this._handleAddonClick,
      get_host_offset: _this._getHostOffset,
      unload: _this._handleUnload
    };
    return _this;
  }

  _createClass(XDMRPC, [{
    key: "_padUndefinedArguments",
    value: function _padUndefinedArguments(array, length) {
      return array.length >= length ? array : array.concat(new Array(length - array.length));
    }
  }, {
    key: "_verifyAPI",
    value: function _verifyAPI(event, reg) {
      var untrustedTargets = event.data.targets;

      if (!untrustedTargets) {
        return;
      }

      var trustedSpec = this.getApiSpec();
      var tampered = false;

      function check(trusted, untrusted) {
        Object.getOwnPropertyNames(untrusted).forEach(function (name) {
          if (_typeof(untrusted[name]) === 'object' && trusted[name]) {
            check(trusted[name], untrusted[name]);
          } else {
            if (untrusted[name] === 'parent' && trusted[name]) {
              tampered = true;
            }
          }
        });
      }

      check(trustedSpec, untrustedTargets);

      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          type: 'api_tamper',
          tampered: tampered
        }, reg.extension.url);
      } else {
        console.warn("_verifyAPI postMessage skipped as event source missing.");
      }
    }
  }, {
    key: "_handleInit",
    value: function _handleInit(event, reg) {
      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          type: 'init_received'
        }, reg.extension.url);
      } else {
        console.warn("_handleInit postMessage skipped as event source missing.");
      }

      this._registeredExtensions[reg.extension_id].source = event.source;

      if (reg.initCallback) {
        reg.initCallback(event.data.eid);
        delete reg.initCallback;
      }

      if (event.data.targets) {
        this._verifyAPI(event, reg);
      }
    }
  }, {
    key: "_getHostOffset",
    value: function _getHostOffset(event, _window) {
      var hostWindow = event.source;
      var hostFrameOffset = null;
      var windowReference = _window || window; // For testing

      if (windowReference === windowReference.top && typeof windowReference.getHostOffsetFunctionOverride === 'function') {
        hostFrameOffset = windowReference.getHostOffsetFunctionOverride(hostWindow);
      }

      if (typeof hostFrameOffset !== 'number') {
        hostFrameOffset = 0; // Find the closest frame that has the same origin as event source

        while (!this._hasSameOrigin(hostWindow)) {
          // Climb up the iframe tree 1 layer
          hostFrameOffset++;
          hostWindow = hostWindow.parent;
        }
      }

      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          hostFrameOffset: hostFrameOffset
        }, event.origin);
      } else {
        console.warn("_getHostOffset postMessage skipped as event source missing.");
      }
    }
  }, {
    key: "_hasSameOrigin",
    value: function _hasSameOrigin(window) {
      if (window === window.top) {
        return true;
      }

      try {
        // Try set & read a variable on the given window
        // If we can successfully read the value then it means the given window has the same origin
        // as the window that is currently executing the script
        var testVariableName = 'test_var_' + Math.random().toString(16).substr(2);
        window[testVariableName] = true;
        return window[testVariableName];
      } catch (e) {// A exception will be thrown if the windows doesn't have the same origin
      }

      return false;
    }
  }, {
    key: "_handleResponse",
    value: function _handleResponse(event) {
      var data = event.data;
      var pendingCallback = this._pendingCallbacks[data.mid];

      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];
        pendingCallback.apply(window, data.args);
      }
    }
  }, {
    key: "registerRequestNotifier",
    value: function registerRequestNotifier(cb) {
      this._registeredRequestNotifier = cb;
    }
  }, {
    key: "_handleRequest",
    value: function _handleRequest(event, reg) {
      function sendResponse() {
        var args = _util.default.sanitizeStructuredClone(_util.default.argumentsToArray(arguments));

        if (event.source && event.source.postMessage) {
          // only post a message if the source of the event still exists
          event.source.postMessage({
            mid: event.data.mid,
            type: 'resp',
            forPlugin: true,
            args: args
          }, reg.extension.url);
        } else {
          console.warn("_handleRequest postMessage skipped as event source missing.");
        }
      }

      var data = event.data;
      var module = this._registeredAPIModules[data.mod];
      var extension = this.getRegisteredExtensions(reg.extension)[0];

      if (module) {
        var fnName = data.fn;

        if (data._cls) {
          var Cls = module[data._cls];
          var ns = data.mod + '-' + data._cls + '-';
          sendResponse._id = data._id;

          if (fnName === 'constructor') {
            if (!Cls._construct) {
              Cls.constructor.prototype._destroy = function () {
                delete this._context._proxies[ns + this._id];
              };

              Cls._construct = function () {
                for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
                  args[_key] = arguments[_key];
                }

                var inst = _construct(Cls.constructor, args);

                var callback = args[args.length - 1];
                inst._id = callback._id;
                inst._context = callback._context;
                inst._context._proxies[ns + inst._id] = inst;
                return inst;
              };
            }

            module = Cls;
            fnName = '_construct';
          } else {
            module = extension._proxies[ns + data._id];
          }
        }

        var method = module[fnName];

        if (method) {
          var methodArgs = data.args;
          var padLength = method.length - 1;

          if (fnName === '_construct') {
            padLength = module.constructor.length - 1;
          }

          sendResponse._context = extension;
          methodArgs = this._padUndefinedArguments(methodArgs, padLength);
          methodArgs.push(sendResponse);
          var promiseResult = method.apply(module, methodArgs);

          if (method.returnsPromise) {
            if (!(_typeof(promiseResult) === 'object' || typeof promiseResult === 'function') || typeof promiseResult.then !== 'function') {
              sendResponse('Defined module method did not return a promise.');
            } else {
              promiseResult.then(function (result) {
                sendResponse(undefined, result);
              }).catch(function (err) {
                err = err instanceof Error ? err.message : err;
                sendResponse(err);
              });
            }
          }

          if (this._registeredRequestNotifier) {
            this._registeredRequestNotifier.call(null, {
              module: data.mod,
              fn: data.fn,
              type: data.type,
              args: methodArgs,
              addon_key: reg.extension.addon_key,
              key: reg.extension.key,
              extension_id: reg.extension_id
            });
          }
        }
      }
    }
  }, {
    key: "_handleBroadcast",
    value: function _handleBroadcast(event, reg) {
      var event_data = event.data;

      var targetSpec = function targetSpec(r) {
        return r.extension.addon_key === reg.extension.addon_key && r.extension_id !== reg.extension_id;
      };

      this.dispatch(event_data.etyp, targetSpec, event_data.evnt, null, null);
    }
  }, {
    key: "_handleKeyTriggered",
    value: function _handleKeyTriggered(event, reg) {
      var eventData = event.data;

      var keycodeEntry = this._keycodeKey(eventData.keycode, eventData.modifiers, reg.extension_id);

      var listeners = this._keycodeCallbacks[keycodeEntry];

      if (listeners) {
        listeners.forEach(function (listener) {
          listener.call(null, {
            addon_key: reg.extension.addon_key,
            key: reg.extension.key,
            extension_id: reg.extension_id,
            keycode: eventData.keycode,
            modifiers: eventData.modifiers
          });
        }, this);
      }
    }
  }, {
    key: "defineAPIModule",
    value: function defineAPIModule(module, moduleName) {
      moduleName = moduleName || '_globals';
      this._registeredAPIModules[moduleName] = _util.default.extend({}, this._registeredAPIModules[moduleName] || {}, module);
      return this._registeredAPIModules;
    }
  }, {
    key: "isAPIModuleDefined",
    value: function isAPIModuleDefined(moduleName) {
      return typeof this._registeredAPIModules[moduleName] !== 'undefined';
    }
  }, {
    key: "_pendingEventKey",
    value: function _pendingEventKey(targetSpec, time) {
      var key = targetSpec.addon_key || 'global';

      if (targetSpec.key) {
        key = "".concat(key, "@@").concat(targetSpec.key);
      }

      key = "".concat(key, "@@").concat(time);
      return key;
    }
  }, {
    key: "queueEvent",
    value: function queueEvent(type, targetSpec, event, callback) {
      var loaded_frame,
          targets = this._findRegistrations(targetSpec);

      loaded_frame = targets.some(function (target) {
        return target.registered_events !== undefined;
      }, this);

      if (loaded_frame) {
        this.dispatch(type, targetSpec, event, callback);
      } else {
        this._cleanupInvalidEvents();

        var time = new Date().getTime();
        this._pendingEvents[this._pendingEventKey(targetSpec, time)] = {
          type: type,
          targetSpec: targetSpec,
          event: event,
          callback: callback,
          time: time,
          uid: _util.default.randomString()
        };
      }
    }
  }, {
    key: "_cleanupInvalidEvents",
    value: function _cleanupInvalidEvents() {
      var _this2 = this;

      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this2._pendingEvents[index];
        var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;

        if (!eventIsValid) {
          delete _this2._pendingEvents[index];
        }
      });
    }
  }, {
    key: "_handleEventQuery",
    value: function _handleEventQuery(message, extension) {
      var _this3 = this;

      var executed = {};
      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this3._pendingEvents[index];
        var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;
        var isSameTarget = !element.targetSpec || _this3._findRegistrations(element.targetSpec).length !== 0;

        if (isSameTarget && element.targetSpec.key) {
          isSameTarget = element.targetSpec.addon_key === extension.extension.addon_key && element.targetSpec.key === extension.extension.key;
        }

        if (eventIsValid && isSameTarget) {
          executed[index] = element;
          element.targetSpec = element.targetSpec || {};

          _this3.dispatch(element.type, element.targetSpec, element.event, element.callback, message.source);
        } else if (!eventIsValid) {
          delete _this3._pendingEvents[index];
        }
      });
      this._registeredExtensions[extension.extension_id].registered_events = message.data.args;
      return executed;
    }
  }, {
    key: "_handleUnload",
    value: function _handleUnload(event, reg) {
      if (!reg) {
        return;
      }

      if (reg.extension_id && this._registeredExtensions[reg.extension_id]) {
        delete this._registeredExtensions[reg.extension_id].source;
      }

      if (reg.unloadCallback) {
        reg.unloadCallback(event.data.eid);
      }
    }
  }, {
    key: "dispatch",
    value: function dispatch(type, targetSpec, event, callback, source) {
      function sendEvent(reg, evnt) {
        if (reg.source && reg.source.postMessage) {
          var mid;

          if (callback) {
            mid = _util.default.randomString();
            this._pendingCallbacks[mid] = callback;
          }

          reg.source.postMessage({
            type: 'evt',
            mid: mid,
            etyp: type,
            evnt: evnt
          }, reg.extension.url);
        }
      }

      var registrations = this._findRegistrations(targetSpec || {});

      registrations.forEach(function (reg) {
        if (source && !reg.source) {
          reg.source = source;
        }

        if (reg.source) {
          _util.default._bind(this, sendEvent)(reg, event);
        }
      }, this);
    }
  }, {
    key: "_findRegistrations",
    value: function _findRegistrations(targetSpec) {
      var _this4 = this;

      if (this._registeredExtensions.length === 0) {
        _util.default.error('no registered extensions', this._registeredExtensions);

        return [];
      }

      var keys = Object.getOwnPropertyNames(targetSpec);
      var registrations = Object.getOwnPropertyNames(this._registeredExtensions).map(function (key) {
        return _this4._registeredExtensions[key];
      });

      if (targetSpec instanceof Function) {
        return registrations.filter(targetSpec);
      } else {
        return registrations.filter(function (reg) {
          return keys.every(function (key) {
            return reg.extension[key] === targetSpec[key];
          });
        });
      }
    }
  }, {
    key: "registerExtension",
    value: function registerExtension(extension_id, data) {
      data._proxies = {};
      data.extension_id = extension_id;
      this._registeredExtensions[extension_id] = data;
    }
  }, {
    key: "_keycodeKey",
    value: function _keycodeKey(key, modifiers, extension_id) {
      var code = key;

      if (modifiers) {
        if (typeof modifiers === "string") {
          modifiers = [modifiers];
        }

        modifiers.sort();
        modifiers.forEach(function (modifier) {
          code += '$$' + modifier;
        }, this);
      }

      return code + '__' + extension_id;
    }
  }, {
    key: "registerKeyListener",
    value: function registerKeyListener(extension_id, key, modifiers, callback) {
      if (typeof modifiers === "string") {
        modifiers = [modifiers];
      }

      var reg = this._registeredExtensions[extension_id];

      var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);

      if (!this._keycodeCallbacks[keycodeEntry]) {
        this._keycodeCallbacks[keycodeEntry] = [];
        reg.source.postMessage({
          type: 'key_listen',
          keycode: key,
          modifiers: modifiers,
          action: 'add'
        }, reg.extension.url);
      }

      this._keycodeCallbacks[keycodeEntry].push(callback);
    }
  }, {
    key: "unregisterKeyListener",
    value: function unregisterKeyListener(extension_id, key, modifiers, callback) {
      var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);

      var potentialCallbacks = this._keycodeCallbacks[keycodeEntry];
      var reg = this._registeredExtensions[extension_id];

      if (potentialCallbacks) {
        if (callback) {
          var index = potentialCallbacks.indexOf(callback);

          this._keycodeCallbacks[keycodeEntry].splice(index, 1);
        } else {
          delete this._keycodeCallbacks[keycodeEntry];
        }

        if (reg.source && reg.source.postMessage) {
          reg.source.postMessage({
            type: 'key_listen',
            keycode: key,
            modifiers: modifiers,
            action: 'remove'
          }, reg.extension.url);
        }
      }
    }
  }, {
    key: "registerClickHandler",
    value: function registerClickHandler(callback) {
      if (typeof callback !== 'function') {
        throw new Error('callback must be a function');
      }

      this._clickHandlers.push(callback);
    }
  }, {
    key: "_handleAddonClick",
    value: function _handleAddonClick(event, reg) {
      for (var i = 0; i < this._clickHandlers.length; i++) {
        if (typeof this._clickHandlers[i] === 'function') {
          this._clickHandlers[i]({
            addon_key: reg.extension.addon_key,
            key: reg.extension.key,
            extension_id: reg.extension_id
          });
        }
      }
    }
  }, {
    key: "unregisterClickHandler",
    value: function unregisterClickHandler() {
      this._clickHandlers = [];
    }
  }, {
    key: "getApiSpec",
    value: function getApiSpec(addonKey) {
      var _this5 = this;

      function getModuleDefinition(mod) {
        return Object.getOwnPropertyNames(mod).reduce(function (accumulator, memberName) {
          var member = mod[memberName];

          switch (_typeof(member)) {
            case 'function':
              accumulator[memberName] = {
                args: _util.default.argumentNames(member),
                returnsPromise: member.returnsPromise || false
              };
              break;

            case 'object':
              if (member.hasOwnProperty('constructor')) {
                accumulator[memberName] = getModuleDefinition(member);
              }

              break;
          }

          return accumulator;
        }, {});
      }

      return Object.getOwnPropertyNames(this._registeredAPIModules).reduce(function (accumulator, moduleName) {
        var module = _this5._registeredAPIModules[moduleName];

        if (typeof module.addonKey === 'undefined' || module.addonKey === addonKey) {
          accumulator[moduleName] = getModuleDefinition(module);
        }

        return accumulator;
      }, {});
    }
  }, {
    key: "_originEqual",
    value: function _originEqual(url, origin) {
      function strCheck(str) {
        return typeof str === 'string' && str.length > 0;
      }

      var urlOrigin = _util.default.getOrigin(url); // check strings are strings and they contain something


      if (!strCheck(url) || !strCheck(origin) || !strCheck(urlOrigin)) {
        return false;
      }

      return origin === urlOrigin;
    } // validate origin of postMessage

  }, {
    key: "_checkOrigin",
    value: function _checkOrigin(event, reg) {
      var no_source_types = ['init'];
      var isNoSourceType = reg && !reg.source && no_source_types.indexOf(event.data.type) > -1;
      var sourceTypeMatches = reg && event.source === reg.source;

      var hasExtensionUrl = reg && this._originEqual(reg.extension.url, event.origin);

      var isValidOrigin = hasExtensionUrl && (isNoSourceType || sourceTypeMatches); // get_host_offset fires before init

      if (event.data.type === 'get_host_offset' && window === window.top) {
        isValidOrigin = true;
      } // check undefined for chromium (Issue 395010)


      if (event.data.type === 'unload' && (sourceTypeMatches || event.source === undefined)) {
        isValidOrigin = true;
      }

      return isValidOrigin;
    }
  }, {
    key: "getRegisteredExtensions",
    value: function getRegisteredExtensions(filter) {
      if (filter) {
        return this._findRegistrations(filter);
      }

      return this._registeredExtensions;
    }
  }, {
    key: "unregisterExtension",
    value: function unregisterExtension(filter) {
      var registrations = this._findRegistrations(filter);

      if (registrations.length !== 0) {
        registrations.forEach(function (registration) {
          var _this6 = this;

          var keys = Object.keys(this._pendingEvents);
          keys.forEach(function (index) {
            var element = _this6._pendingEvents[index];
            var targetSpec = element.targetSpec || {};

            if (targetSpec.addon_key === registration.extension.addon_key && targetSpec.key === registration.extension.key) {
              delete _this6._pendingEvents[index];
            }
          });
          delete this._registeredExtensions[registration.extension_id];
        }, this);
      }
    }
  }, {
    key: "setFeatureFlagGetter",
    value: function setFeatureFlagGetter(getBooleanFeatureFlag) {
      this._getBooleanFeatureFlag = getBooleanFeatureFlag;
    }
  }]);

  return XDMRPC;
}(_postmessage.default);

var _default = XDMRPC;
exports.default = _default;
module.exports = exports.default;

},{"../common/postmessage":3,"../common/util":4}],8:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _util = _interopRequireDefault(_dereq_("../common/util"));

var _postmessage = _interopRequireDefault(_dereq_("../common/postmessage"));

var _dollar = _interopRequireDefault(_dereq_("./dollar"));

var _size = _interopRequireDefault(_dereq_("./size"));

var _resizeListener = _interopRequireDefault(_dereq_("./resize-listener"));

var _autoResizeAction = _interopRequireDefault(_dereq_("./auto-resize-action"));

var _configurationOptions = _interopRequireDefault(_dereq_("./configuration-options"));

var _consumerOptions = _interopRequireDefault(_dereq_("./consumer-options"));

var _documentContainer = _interopRequireDefault(_dereq_("./document-container"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var POSSIBLE_MODIFIER_KEYS = ['ctrl', 'shift', 'alt', 'meta'];

var AP = /*#__PURE__*/function (_PostMessage) {
  _inherits(AP, _PostMessage);

  var _super = _createSuper(AP);

  function AP(options) {
    var _this;

    var initCheck = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

    _classCallCheck(this, AP);

    _this = _super.call(this);

    _configurationOptions.default.set(options);

    _this._data = _this._parseInitData();

    _configurationOptions.default.set(_this._data.options);

    _this._data.options = _this._data.options || {};
    _this._hostOrigin = _this._data.options.hostOrigin || '*';
    _this._top = window.top;
    _this._host = window.parent || window;
    _this._topHost = _this._getHostFrame(_this._data.options.hostFrameOffset);

    if (_this._topHost !== _this._top) {
      _this._verifyHostFrameOffset();
    }

    _this._initTimeout = 5000;
    _this._initReceived = false;
    _this._initCheck = initCheck;
    _this._isKeyDownBound = false;
    _this._hostModules = {};
    _this._eventHandlers = {};
    _this._pendingCallbacks = {};
    _this._keyListeners = [];
    _this._version = "v2.3.1";
    _this._apiTampered = undefined;
    _this._isSubIframe = _this._topHost !== window.parent;
    _this._onConfirmedFns = [];
    _this._promise = Promise;

    if (_this._data.api) {
      _this._setupAPI(_this._data.api);

      _this._setupAPIWithoutRequire(_this._data.api);
    }

    _this._messageHandlers = {
      init_received: _this._handleInitReceived,
      resp: _this._handleResponse,
      evt: _this._handleEvent,
      key_listen: _this._handleKeyListen,
      api_tamper: _this._handleApiTamper
    };

    if (_this._data.origin) {
      _this._sendInit(_this._host, _this._data.origin);

      if (_this._isSubIframe) {
        _this._sendInit(_this._topHost, _this._hostOrigin);
      }
    }

    _this._registerOnUnload();

    _this.resize = _util.default._bind(_assertThisInitialized(_this), function (width, height) {
      if (!(0, _documentContainer.default)()) {
        _util.default.warn('resize called before container or body appeared, ignoring');

        return;
      }

      var dimensions = (0, _size.default)();

      if (!width) {
        width = dimensions.w;
      }

      if (!height) {
        height = dimensions.h;
      }

      if (_this._hostModules.env && _this._hostModules.env.resize) {
        _this._hostModules.env.resize(width, height);
      }
    });
    (0, _dollar.default)(_util.default._bind(_assertThisInitialized(_this), _this._autoResizer));
    _this.container = _documentContainer.default;
    _this.size = _size.default;
    window.addEventListener('click', function (e) {
      _this._host.postMessage({
        eid: _this._data.extension_id,
        type: 'addon_clicked'
      }, _this._hostOrigin);
    });
    return _this;
  }

  _createClass(AP, [{
    key: "_getHostFrame",
    value: function _getHostFrame(offset) {
      // Climb up the iframe tree to find the real host
      if (offset && typeof offset === 'number') {
        var hostFrame = window;

        for (var i = 0; i < offset; i++) {
          hostFrame = hostFrame.parent;
        }

        return hostFrame;
      } else {
        return this._top;
      }
    }
  }, {
    key: "_verifyHostFrameOffset",
    value: function _verifyHostFrameOffset() {
      var _this2 = this;

      // Asynchronously verify the host frame option with this._top
      var callback = function callback(e) {
        if (e.source === _this2._top && e.data && typeof e.data.hostFrameOffset === 'number') {
          window.removeEventListener('message', callback);

          if (_this2._getHostFrame(e.data.hostFrameOffset) !== _this2._topHost) {
            _util.default.error('hostFrameOffset tampering detected, setting host frame to top window');

            _this2._topHost = _this2._top;
          }
        }
      };

      window.addEventListener('message', callback);

      this._top.postMessage({
        type: 'get_host_offset'
      }, this._hostOrigin);
    }
  }, {
    key: "_handleApiTamper",
    value: function _handleApiTamper(event) {
      if (event.data.tampered !== false) {
        this._host = undefined;
        this._apiTampered = true;

        _util.default.error('XDM API tampering detected, api disabled');
      } else {
        this._apiTampered = false;

        this._onConfirmedFns.forEach(function (cb) {
          cb.apply(null);
        });
      }

      this._onConfirmedFns = [];
    }
  }, {
    key: "_registerOnUnload",
    value: function _registerOnUnload() {
      _dollar.default.bind(window, 'unload', _util.default._bind(this, function () {
        this._sendUnload(this._host, this._data.origin);

        if (this._isSubIframe) {
          this._sendUnload(this._topHost, this._hostOrigin);
        }
      }));
    }
  }, {
    key: "_sendUnload",
    value: function _sendUnload(frame, origin) {
      frame.postMessage({
        eid: this._data.extension_id,
        type: 'unload'
      }, origin || '*');
    }
  }, {
    key: "_bindKeyDown",
    value: function _bindKeyDown() {
      if (!this._isKeyDownBound) {
        _dollar.default.bind(window, 'keydown', _util.default._bind(this, this._handleKeyDownDomEvent));

        this._isKeyDownBound = true;
      }
    }
  }, {
    key: "_autoResizer",
    value: function _autoResizer() {
      this._enableAutoResize = Boolean(_configurationOptions.default.get('autoresize'));

      if (_consumerOptions.default.get('resize') === false || _consumerOptions.default.get('sizeToParent') === true) {
        this._enableAutoResize = false;
      }

      if (this._enableAutoResize) {
        this._initResize();
      }
    }
    /**
    * The initialization data is passed in when the iframe is created as its 'name' attribute.
    * Example:
    * {
    *   extension_id: The ID of this iframe as defined by the host
    *   origin: 'https://example.org'  // The parent's window origin
    *   api: {
    *     _globals: { ... },
    *     messages = {
    *       clear: {},
    *       ...
    *     },
    *     ...
    *   }
    * }
    **/

  }, {
    key: "_parseInitData",
    value: function _parseInitData(data) {
      try {
        return JSON.parse(data || window.name);
      } catch (e) {
        return {};
      }
    }
  }, {
    key: "_findTarget",
    value: function _findTarget(moduleName, methodName) {
      return this._data.options && this._data.options.targets && this._data.options.targets[moduleName] && this._data.options.targets[moduleName][methodName] ? this._data.options.targets[moduleName][methodName] : 'top';
    }
  }, {
    key: "_createModule",
    value: function _createModule(moduleName, api) {
      var _this3 = this;

      return Object.getOwnPropertyNames(api).reduce(function (accumulator, memberName) {
        var member = api[memberName];

        if (member.hasOwnProperty('constructor')) {
          accumulator[memberName] = _this3._createProxy(moduleName, member, memberName);
        } else {
          accumulator[memberName] = _this3._createMethodHandler({
            mod: moduleName,
            fn: memberName,
            returnsPromise: member.returnsPromise
          });
        }

        return accumulator;
      }, {});
    }
  }, {
    key: "_setupAPI",
    value: function _setupAPI(api) {
      var _this4 = this;

      this._hostModules = Object.getOwnPropertyNames(api).reduce(function (accumulator, moduleName) {
        accumulator[moduleName] = _this4._createModule(moduleName, api[moduleName], api[moduleName]._options);
        return accumulator;
      }, {});
      Object.getOwnPropertyNames(this._hostModules._globals || {}).forEach(function (global) {
        _this4[global] = _this4._hostModules._globals[global];
      });
    }
  }, {
    key: "_setupAPIWithoutRequire",
    value: function _setupAPIWithoutRequire(api) {
      var _this5 = this;

      Object.getOwnPropertyNames(api).forEach(function (moduleName) {
        if (typeof _this5[moduleName] !== "undefined") {
          throw new Error('XDM module: ' + moduleName + ' will collide with existing variable');
        }

        _this5[moduleName] = _this5._createModule(moduleName, api[moduleName]);
      }, this);
    }
  }, {
    key: "_pendingCallback",
    value: function _pendingCallback(mid, fn, metaData) {
      if (metaData) {
        Object.getOwnPropertyNames(metaData).forEach(function (metaDataName) {
          fn[metaDataName] = metaData[metaDataName];
        });
      }

      this._pendingCallbacks[mid] = fn;
    }
  }, {
    key: "_createProxy",
    value: function _createProxy(moduleName, api, className) {
      var module = this._createModule(moduleName, api);

      function Cls(args) {
        if (!(this instanceof Cls)) {
          return new Cls(arguments);
        }

        this._cls = className;
        this._id = _util.default.randomString();
        module.constructor.apply(this, args);
        return this;
      }

      Object.getOwnPropertyNames(module).forEach(function (methodName) {
        if (methodName !== 'constructor') {
          Cls.prototype[methodName] = module[methodName];
        }
      });
      return Cls;
    }
  }, {
    key: "_createMethodHandler",
    value: function _createMethodHandler(methodData) {
      var that = this;
      return function () {
        var args = _util.default.argumentsToArray(arguments);

        var data = {
          eid: that._data.extension_id,
          type: 'req',
          mod: methodData.mod,
          fn: methodData.fn
        };
        var targetOrigin;
        var target;
        var xdmPromise;

        var mid = _util.default.randomString();

        if (that._findTarget(methodData.mod, methodData.fn) === 'top') {
          target = that._topHost;
          targetOrigin = that._hostOrigin;
        } else {
          target = that._host;
          targetOrigin = that._data.origin;
        }

        if (_util.default.hasCallback(args)) {
          data.mid = mid;

          that._pendingCallback(data.mid, args.pop(), {
            useCallback: true,
            isPromiseMethod: Boolean(methodData.returnsPromise)
          });
        } else if (methodData.returnsPromise) {
          data.mid = mid;
          xdmPromise = new Promise(function (resolve, reject) {
            that._pendingCallback(data.mid, function (err, result) {
              if (err || typeof result === 'undefined' && typeof err === 'undefined') {
                reject(err);
              } else {
                resolve(result);
              }
            }, {
              useCallback: false,
              isPromiseMethod: Boolean(methodData.returnsPromise)
            });
          });
          xdmPromise.catch(function (err) {
            _util.default.warn("Failed promise: ".concat(err));
          });
        }

        if (this && this._cls) {
          data._cls = this._cls;
          data._id = this._id;
        }

        data.args = _util.default.sanitizeStructuredClone(args);

        if (that._isSubIframe && typeof that._apiTampered === 'undefined') {
          that._onConfirmedFns.push(function () {
            target.postMessage(data, targetOrigin);
          });
        } else {
          target.postMessage(data, targetOrigin);
        }

        if (xdmPromise) {
          return xdmPromise;
        }
      };
    }
  }, {
    key: "_handleResponse",
    value: function _handleResponse(event) {
      var data = event.data;

      if (!data.forPlugin) {
        return;
      }

      var pendingCallback = this._pendingCallbacks[data.mid];

      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];

        try {
          // Promise methods always return error result as first arg
          // If a promise method is invoked using callbacks, strip first arg.
          if (pendingCallback.useCallback && pendingCallback.isPromiseMethod) {
            data.args.shift();
          }

          pendingCallback.apply(window, data.args);
        } catch (e) {
          _util.default.error(e.message, e.stack);
        }
      }
    }
  }, {
    key: "_handleEvent",
    value: function _handleEvent(event) {
      var sendResponse = function sendResponse() {
        var args = _util.default.argumentsToArray(arguments);

        event.source.postMessage({
          eid: this._data.extension_id,
          mid: event.data.mid,
          type: 'resp',
          args: args
        }, this._data.origin);
      };

      var data = event.data;
      sendResponse = _util.default._bind(this, sendResponse);
      sendResponse._context = {
        eventName: data.etyp
      };

      function toArray(handlers) {
        if (handlers) {
          if (!Array.isArray(handlers)) {
            handlers = [handlers];
          }

          return handlers;
        }

        return [];
      }

      var handlers = toArray(this._eventHandlers[data.etyp]);
      handlers = handlers.concat(toArray(this._eventHandlers._any));
      handlers.forEach(function (handler) {
        try {
          handler(data.evnt, sendResponse);
        } catch (e) {
          _util.default.error('exception thrown in event callback for:' + data.etyp);
        }
      }, this);

      if (data.mid) {
        sendResponse();
      }
    }
  }, {
    key: "_handleKeyDownDomEvent",
    value: function _handleKeyDownDomEvent(event) {
      var modifiers = [];
      POSSIBLE_MODIFIER_KEYS.forEach(function (modifierKey) {
        if (event[modifierKey + 'Key']) {
          modifiers.push(modifierKey);
        }
      }, this);

      var keyListenerId = this._keyListenerId(event.keyCode, modifiers);

      var requestedKey = this._keyListeners.indexOf(keyListenerId);

      if (requestedKey >= 0) {
        this._host.postMessage({
          eid: this._data.extension_id,
          keycode: event.keyCode,
          modifiers: modifiers,
          type: 'key_triggered'
        }, this._data.origin);
      }
    }
  }, {
    key: "_keyListenerId",
    value: function _keyListenerId(keycode, modifiers) {
      var keyListenerId = keycode;

      if (modifiers) {
        if (typeof modifiers === "string") {
          modifiers = [modifiers];
        }

        modifiers.sort();
        modifiers.forEach(function (modifier) {
          keyListenerId += '$$' + modifier;
        }, this);
      }

      return keyListenerId;
    }
  }, {
    key: "_handleKeyListen",
    value: function _handleKeyListen(postMessageEvent) {
      var keyListenerId = this._keyListenerId(postMessageEvent.data.keycode, postMessageEvent.data.modifiers);

      if (postMessageEvent.data.action === "remove") {
        var index = this._keyListeners.indexOf(keyListenerId);

        this._keyListeners.splice(index, 1);
      } else if (postMessageEvent.data.action === "add") {
        // only bind onKeyDown once a key is registered.
        this._bindKeyDown();

        this._keyListeners.push(keyListenerId);
      }
    }
  }, {
    key: "_checkOrigin",
    value: function _checkOrigin(event) {
      var no_source_types = ['api_tamper'];

      if (event.data && no_source_types.indexOf(event.data.type) > -1) {
        return true;
      }

      if (this._isSubIframe && event.source === this._topHost) {
        return true;
      }

      return event.origin === this._data.origin && event.source === this._host;
    }
  }, {
    key: "_handleInitReceived",
    value: function _handleInitReceived() {
      this._initReceived = true;
    }
  }, {
    key: "_sendInit",
    value: function _sendInit(frame, origin) {
      var _this6 = this;

      var targets;

      if (frame === this._topHost && this._topHost !== window.parent) {
        targets = _configurationOptions.default.get('targets');
      }

      frame.postMessage({
        eid: this._data.extension_id,
        type: 'init',
        targets: targets
      }, origin || '*');
      this._initCheck && this._data.options.globalOptions.check_init && setTimeout(function () {
        if (!_this6._initReceived) {
          throw new Error("Initialization message not received");
        }
      }, this._initTimeout);
    }
  }, {
    key: "broadcast",
    value: function broadcast(event, evnt) {
      if (!_util.default.isString(event)) {
        throw new Error("Event type must be string");
      }

      this._host.postMessage({
        eid: this._data.extension_id,
        type: 'broadcast',
        etyp: event,
        evnt: evnt
      }, this._data.origin);
    }
  }, {
    key: "require",
    value: function _dereq_(modules, callback) {
      var _this7 = this;

      var requiredModules = Array.isArray(modules) ? modules : [modules],
          args = requiredModules.map(function (module) {
        return _this7._hostModules[module] || _this7._hostModules._globals[module];
      });
      callback.apply(window, args);
    }
  }, {
    key: "register",
    value: function register(handlers) {
      if (_typeof(handlers) === "object") {
        this._eventHandlers = _objectSpread(_objectSpread({}, this._eventHandlers), handlers) || {};

        this._host.postMessage({
          eid: this._data.extension_id,
          type: 'event_query',
          args: Object.getOwnPropertyNames(handlers)
        }, this._data.origin);
      }
    }
  }, {
    key: "registerAny",
    value: function registerAny(handlers) {
      this.register({
        '_any': handlers
      });
    }
  }, {
    key: "_initResize",
    value: function _initResize() {
      var _this8 = this;

      requestAnimationFrame(function () {
        return _this8.resize();
      });
      var autoresize = new _autoResizeAction.default(this.resize);

      _resizeListener.default.add(_util.default._bind(autoresize, autoresize.triggered));
    }
  }]);

  return AP;
}(_postmessage.default);

var _default = AP;
exports.default = _default;
module.exports = exports.default;

},{"../common/postmessage":3,"../common/util":4,"./auto-resize-action":9,"./configuration-options":10,"./consumer-options":11,"./document-container":12,"./dollar":13,"./resize-listener":14,"./size":15}],9:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _size = _interopRequireDefault(_dereq_("./size"));

var _util = _interopRequireDefault(_dereq_("../common/util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AutoResizeAction = /*#__PURE__*/function () {
  function AutoResizeAction(callback) {
    _classCallCheck(this, AutoResizeAction);

    this.resizeError = _util.default.throttle(function (msg) {
      console.info(msg);
    }, 1000);
    this.dimensionStores = {
      width: [],
      height: []
    };
    this.callback = callback;
  }

  _createClass(AutoResizeAction, [{
    key: "_setVal",
    value: function _setVal(val, type, time) {
      this.dimensionStores[type] = this.dimensionStores[type].filter(function (entry) {
        return time - entry.setAt < 400;
      });
      this.dimensionStores[type].push({
        val: parseInt(val, 10),
        setAt: time
      });
    }
  }, {
    key: "_isFlicker",
    value: function _isFlicker(val, type) {
      return this.dimensionStores[type].length >= 5;
    }
  }, {
    key: "triggered",
    value: function triggered(dimensions) {
      dimensions = dimensions || (0, _size.default)();
      var now = Date.now();

      this._setVal(dimensions.w, 'width', now);

      this._setVal(dimensions.h, 'height', now);

      var isFlickerWidth = this._isFlicker(dimensions.w, 'width', now);

      var isFlickerHeight = this._isFlicker(dimensions.h, 'height', now);

      if (isFlickerWidth) {
        dimensions.w = "100%";
        this.resizeError("SIMPLE XDM: auto resize flickering width detected, setting to 100%");
      }

      if (isFlickerHeight) {
        var vals = this.dimensionStores['height'].map(function (x) {
          return x.val;
        });
        dimensions.h = Math.max.apply(null, vals) + 'px';
        this.resizeError("SIMPLE XDM: auto resize flickering height detected, setting to: " + dimensions.h);
      }

      this.callback(dimensions.w, dimensions.h);
    }
  }]);

  return AutoResizeAction;
}();

var _default = AutoResizeAction;
exports.default = _default;
module.exports = exports.default;

},{"../common/util":4,"./size":15}],10:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
* Extension wide configuration values
*/
var ConfigurationOptions = /*#__PURE__*/function () {
  function ConfigurationOptions() {
    _classCallCheck(this, ConfigurationOptions);

    this.options = {};
  }

  _createClass(ConfigurationOptions, [{
    key: "_flush",
    value: function _flush() {
      this.options = {};
    }
  }, {
    key: "get",
    value: function get(item) {
      return item ? this.options[item] : this.options;
    }
  }, {
    key: "set",
    value: function set(data, value) {
      var _this = this;

      if (!data) {
        return;
      }

      if (value) {
        data = _defineProperty({}, data, value);
      }

      var keys = Object.getOwnPropertyNames(data);
      keys.forEach(function (key) {
        _this.options[key] = data[key];
      }, this);
    }
  }]);

  return ConfigurationOptions;
}();

var _default = new ConfigurationOptions();

exports.default = _default;
module.exports = exports.default;

},{}],11:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _dollar = _interopRequireDefault(_dereq_("./dollar"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ConsumerOptions = /*#__PURE__*/function () {
  function ConsumerOptions() {
    _classCallCheck(this, ConsumerOptions);
  }

  _createClass(ConsumerOptions, [{
    key: "_elementExists",
    value: function _elementExists($el) {
      return $el && $el.length === 1;
    }
  }, {
    key: "_elementOptions",
    value: function _elementOptions($el) {
      return $el.attr("data-options");
    }
  }, {
    key: "_getConsumerOptions",
    value: function _getConsumerOptions() {
      var options = {},
          $optionElement = (0, _dollar.default)("#ac-iframe-options"),
          $scriptElement = (0, _dollar.default)("script[src*='/atlassian-connect/all']"),
          $cdnScriptElement = (0, _dollar.default)("script[src*='/connect-cdn.atl-paas.net/all']");

      if (!this._elementExists($optionElement) || !this._elementOptions($optionElement)) {
        if (this._elementExists($scriptElement)) {
          $optionElement = $scriptElement;
        } else if (this._elementExists($cdnScriptElement)) {
          $optionElement = $cdnScriptElement;
        }
      }

      if (this._elementExists($optionElement)) {
        // get its data-options attribute, if any
        var optStr = this._elementOptions($optionElement);

        if (optStr) {
          // if found, parse the value into kv pairs following the format of a style element
          optStr.split(";").forEach(function (nvpair) {
            nvpair = nvpair.trim();

            if (nvpair) {
              var nv = nvpair.split(":"),
                  k = nv[0].trim(),
                  v = nv[1].trim();

              if (k && v != null) {
                options[k] = v === "true" || v === "false" ? v === "true" : v;
              }
            }
          });
        }
      }

      return options;
    }
  }, {
    key: "_flush",
    value: function _flush() {
      delete this._options;
    }
  }, {
    key: "get",
    value: function get(key) {
      if (!this._options) {
        this._options = this._getConsumerOptions();
      }

      if (key) {
        return this._options[key];
      }

      return this._options;
    }
  }]);

  return ConsumerOptions;
}();

var _default = new ConsumerOptions();

exports.default = _default;
module.exports = exports.default;

},{"./dollar":13}],12:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _dollar = _interopRequireDefault(_dereq_("./dollar"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getContainer() {
  // Look for these two selectors first... you need these to allow for the auto-shrink to work
  // Otherwise, it'll default to document.body which can't auto-grow or auto-shrink
  var container = (0, _dollar.default)('.ac-content, #content');
  return container.length > 0 ? container[0] : document.body;
}

var _default = getContainer;
exports.default = _default;
module.exports = exports.default;

},{"./dollar":13}],13:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _util = _interopRequireDefault(_dereq_("../common/util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _each = _util.default.each,
    document = window.document;

function $(sel, context) {
  context = context || document;
  var els = [];

  if (sel) {
    if (typeof sel === 'string') {
      var results = context.querySelectorAll(sel),
          arr_results = Array.prototype.slice.call(results);
      Array.prototype.push.apply(els, arr_results);
    } else if (sel.nodeType === 1) {
      els.push(sel);
    } else if (sel === window) {
      els.push(sel);
    } else if (typeof sel === 'function') {
      $.onDomLoad(sel);
    }
  }

  _util.default.extend(els, {
    each: function each(it) {
      _each(this, it);

      return this;
    },
    bind: function bind(name, callback) {
      this.each(function (i, el) {
        this.bind(el, name, callback);
      });
    },
    attr: function attr(k) {
      var v;
      this.each(function (i, el) {
        v = el[k] || el.getAttribute && el.getAttribute(k);
        return !v;
      });
      return v;
    },
    removeClass: function removeClass(className) {
      return this.each(function (i, el) {
        if (el.className) {
          el.className = el.className.replace(new RegExp('(^|\\s)' + className + '(\\s|$)'), ' ');
        }
      });
    },
    html: function html(_html) {
      return this.each(function (i, el) {
        el.innerHTML = _html;
      });
    },
    append: function append(spec) {
      return this.each(function (i, to) {
        var el = context.createElement(spec.tag);

        _each(spec, function (k, v) {
          if (k === '$text') {
            if (el.styleSheet) {
              // style tags in ie
              el.styleSheet.cssText = v;
            } else {
              el.appendChild(context.createTextNode(v));
            }
          } else if (k !== 'tag') {
            el[k] = v;
          }
        });

        to.appendChild(el);
      });
    }
  });

  return els;
}

function binder(std, odd) {
  std += 'EventListener';
  odd += 'Event';
  return function (el, e, fn) {
    if (el[std]) {
      el[std](e, fn, false);
    } else if (el[odd]) {
      el[odd]('on' + e, fn);
    }
  };
}

$.bind = binder('add', 'attach');
$.unbind = binder('remove', 'detach');

$.onDomLoad = function (func) {
  var w = window,
      readyState = w.document.readyState;

  if (readyState === "complete") {
    func.call(w);
  } else {
    $.bind(w, "load", function () {
      func.call(w);
    });
  }
};

var _default = $;
exports.default = _default;
module.exports = exports.default;

},{"../common/util":4}],14:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _documentContainer = _interopRequireDefault(_dereq_("./document-container"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function EventQueue() {
  this.q = [];

  this.add = function (ev) {
    this.q.push(ev);
  };

  var i, j;

  this.call = function () {
    for (i = 0, j = this.q.length; i < j; i++) {
      this.q[i].call();
    }
  };
}

function attachResizeEvent(element, resized) {
  if (!element.resizedAttached) {
    element.resizedAttached = new EventQueue();
    element.resizedAttached.add(resized);
  } else if (element.resizedAttached) {
    element.resizedAttached.add(resized);
    return;
  } // padding / margins on the body causes numerous resizing bugs.


  if (element.nodeName === 'BODY') {
    ['padding', 'margin'].forEach(function (attr) {
      element.style[attr + '-bottom'] = '0px';
      element.style[attr + '-top'] = '0px';
    }, this);
  }

  element.resizeSensor = document.createElement('div');
  element.resizeSensor.className = 'ac-resize-sensor';
  var style = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
  var styleChild = 'position: absolute; left: 0; top: 0;';
  element.resizeSensor.style.cssText = style;
  var expand = document.createElement('div');
  expand.className = "ac-resize-sensor-expand";
  expand.style.cssText = style;
  var expandChild = document.createElement('div');
  expand.appendChild(expandChild);
  expandChild.style.cssText = styleChild;
  var shrink = document.createElement('div');
  shrink.className = "ac-resize-sensor-shrink";
  shrink.style.cssText = style;
  var shrinkChild = document.createElement('div');
  shrink.appendChild(shrinkChild);
  shrinkChild.style.cssText = styleChild + ' width: 200%; height: 200%';
  element.resizeSensor.appendChild(expand);
  element.resizeSensor.appendChild(shrink);
  element.appendChild(element.resizeSensor); // https://bugzilla.mozilla.org/show_bug.cgi?id=548397
  // do not set body to relative

  if (element.nodeName !== 'BODY' && window.getComputedStyle && window.getComputedStyle(element).position === 'static') {
    element.style.position = 'relative';
  }

  var lastWidth, lastHeight;

  var reset = function reset() {
    expandChild.style.width = expand.offsetWidth + 10 + 'px';
    expandChild.style.height = expand.offsetHeight + 10 + 'px';
    expand.scrollLeft = expand.scrollWidth;
    expand.scrollTop = expand.scrollHeight;
    shrink.scrollLeft = shrink.scrollWidth;
    shrink.scrollTop = shrink.scrollHeight;
    lastWidth = element.offsetWidth;
    lastHeight = element.offsetHeight;
  };

  reset();

  var changed = function changed() {
    if (element.resizedAttached) {
      element.resizedAttached.call();
    }
  };

  var onScroll = function onScroll() {
    if (element.offsetWidth !== lastWidth || element.offsetHeight !== lastHeight) {
      changed();
    }

    reset();
  };

  expand.addEventListener('scroll', onScroll);
  shrink.addEventListener('scroll', onScroll);
  var observerConfig = {
    attributes: true,
    attributeFilter: ['style']
  };
  var observer = new MutationObserver(onScroll);
  element.resizeObserver = observer;
  observer.observe(element, observerConfig);
}

var _default = {
  add: function add(fn) {
    var container = (0, _documentContainer.default)();
    attachResizeEvent(container, fn);
  },
  remove: function remove() {
    var container = (0, _documentContainer.default)();

    if (container.resizeSensor) {
      container.resizeObserver.disconnect();
      container.removeChild(container.resizeSensor);
      delete container.resizeSensor;
      delete container.resizedAttached;
    }
  }
};
exports.default = _default;
module.exports = exports.default;

},{"./document-container":12}],15:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _documentContainer = _interopRequireDefault(_dereq_("./document-container"));

var _configurationOptions = _interopRequireDefault(_dereq_("./configuration-options"));

var _util = _interopRequireDefault(_dereq_("../common/util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var size = function size(width, height, container) {
  var verticalScrollbarWidth = function verticalScrollbarWidth() {
    var sbWidth = window.innerWidth - container.clientWidth; // sanity check only

    sbWidth = sbWidth < 0 ? 0 : sbWidth;
    sbWidth = sbWidth > 50 ? 50 : sbWidth;
    return sbWidth;
  };

  var horizontalScrollbarHeight = function horizontalScrollbarHeight() {
    var sbHeight = window.innerHeight - Math.min(container.clientHeight, document.documentElement.clientHeight); // sanity check only

    sbHeight = sbHeight < 0 ? 0 : sbHeight;
    sbHeight = sbHeight > 50 ? 50 : sbHeight;
    return sbHeight;
  };

  var w = width == null ? '100%' : width,
      h,
      docHeight;
  var widthInPx = Boolean(_configurationOptions.default.get('widthinpx'));
  container = container || (0, _documentContainer.default)();

  if (!container) {
    _util.default.warn('size called before container or body appeared, ignoring');
  }

  if (widthInPx && typeof w === "string" && w.search('%') !== -1) {
    w = Math.max(container.scrollWidth, container.offsetWidth, container.clientWidth);
  }

  if (height) {
    h = height;
  } else {
    // Determine height of document element
    docHeight = Math.max(container.scrollHeight, document.documentElement.scrollHeight, container.offsetHeight, document.documentElement.offsetHeight, container.clientHeight, document.documentElement.clientHeight);

    if (container === document.body) {
      h = docHeight;
    } else {
      var computed = window.getComputedStyle(container);
      h = container.getBoundingClientRect().height;

      if (h === 0) {
        h = docHeight;
      } else {
        var additionalProperties = ['margin-top', 'margin-bottom'];
        additionalProperties.forEach(function (property) {
          var floated = parseFloat(computed[property]);
          h += floated;
        });
      }
    }
  } // Include iframe scroll bars if visible and using exact dimensions


  w = typeof w === 'number' && Math.min(container.scrollHeight, document.documentElement.scrollHeight) > Math.min(container.clientHeight, document.documentElement.clientHeight) ? w + verticalScrollbarWidth() : w;
  h = typeof h === 'number' && container.scrollWidth > container.clientWidth ? h + horizontalScrollbarHeight() : h;
  return {
    w: w,
    h: h
  };
};

var _default = size;
exports.default = _default;
module.exports = exports.default;

},{"../common/util":4,"./configuration-options":10,"./document-container":12}]},{},[2])(2)
});