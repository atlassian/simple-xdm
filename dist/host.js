(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.host = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _util = _interopRequireDefault(_dereq_("./util"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var PostMessage = /*#__PURE__*/function () {
  function PostMessage(data) {
    _classCallCheck(this, PostMessage);

    var d = data || {};

    this._registerListener(d.listenOn);
  }

  _createClass(PostMessage, [{
    key: "_registerListener",
    value: function _registerListener(listenOn) {
      if (!listenOn || !listenOn.addEventListener) {
        listenOn = window;
      }

      listenOn.addEventListener("message", _util.default._bind(this, this._receiveMessage), false);
    }
  }, {
    key: "_receiveMessage",
    value: function _receiveMessage(event) {
      var handler = this._messageHandlers[event.data.type],
          extensionId = event.data.eid,
          reg;

      if (extensionId && this._registeredExtensions) {
        reg = this._registeredExtensions[extensionId];
      }

      if (!handler || !this._checkOrigin(event, reg)) {
        return false;
      }

      handler.call(this, event, reg);
    }
  }]);

  return PostMessage;
}();

var _default = PostMessage;
exports.default = _default;
module.exports = exports.default;

},{"./util":2}],2:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var LOG_PREFIX = "[Simple-XDM] ";
var nativeBind = Function.prototype.bind;
var util = {
  locationOrigin: function locationOrigin() {
    if (!window.location.origin) {
      return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    } else {
      return window.location.origin;
    }
  },
  randomString: function randomString() {
    return Math.floor(Math.random() * 1000000000).toString(16);
  },
  isString: function isString(str) {
    return typeof str === "string" || str instanceof String;
  },
  argumentsToArray: function argumentsToArray(arrayLike) {
    return Array.prototype.slice.call(arrayLike);
  },
  argumentNames: function argumentNames(fn) {
    return fn.toString().replace(/((\/\/.*$)|(\/\*[^]*?\*\/))/mg, '') // strip comments
    .replace(/[^(]+\(([^)]*)[^]+/, '$1') // get signature
    .match(/([^\s,]+)/g) || [];
  },
  hasCallback: function hasCallback(args) {
    var length = args.length;
    return length > 0 && typeof args[length - 1] === 'function';
  },
  error: function error(msg) {
    if (window.console && window.console.error) {
      var outputError = [];

      if (typeof msg === "string") {
        outputError.push(LOG_PREFIX + msg);
        outputError = outputError.concat(Array.prototype.slice.call(arguments, 1));
      } else {
        outputError.push(LOG_PREFIX);
        outputError = outputError.concat(Array.prototype.slice.call(arguments));
      }

      window.console.error.apply(null, outputError);
    }
  },
  warn: function warn(msg) {
    if (window.console) {
      console.warn(LOG_PREFIX + msg);
    }
  },
  log: function log(msg) {
    if (window.console) {
      window.console.log(LOG_PREFIX + msg);
    }
  },
  _bind: function _bind(thisp, fn) {
    if (nativeBind && fn.bind === nativeBind) {
      return fn.bind(thisp);
    }

    return function () {
      return fn.apply(thisp, arguments);
    };
  },
  throttle: function throttle(func, wait, context) {
    var previous = 0;
    return function () {
      var now = Date.now();

      if (now - previous > wait) {
        previous = now;
        func.apply(context, arguments);
      }
    };
  },
  each: function each(list, iteratee) {
    var length;
    var key;

    if (list) {
      length = list.length;

      if (length != null && typeof list !== 'function') {
        key = 0;

        while (key < length) {
          if (iteratee.call(list[key], key, list[key]) === false) {
            break;
          }

          key += 1;
        }
      } else {
        for (key in list) {
          if (list.hasOwnProperty(key)) {
            if (iteratee.call(list[key], key, list[key]) === false) {
              break;
            }
          }
        }
      }
    }
  },
  extend: function extend(dest) {
    var args = arguments;
    var srcs = [].slice.call(args, 1, args.length);
    srcs.forEach(function (source) {
      if (_typeof(source) === "object") {
        Object.getOwnPropertyNames(source).forEach(function (name) {
          dest[name] = source[name];
        });
      }
    });
    return dest;
  },
  sanitizeStructuredClone: function sanitizeStructuredClone(object) {
    var whiteList = [Boolean, String, Date, RegExp, Blob, File, FileList, ArrayBuffer];
    var blackList = [Error, Node];
    var warn = util.warn;
    var visitedObjects = [];

    function _clone(value) {
      if (typeof value === 'function') {
        warn("A function was detected and removed from the message.");
        return null;
      }

      if (blackList.some(function (t) {
        if (value instanceof t) {
          warn("".concat(t.name, " object was detected and removed from the message."));
          return true;
        }

        return false;
      })) {
        return {};
      }

      if (value && _typeof(value) === 'object' && whiteList.every(function (t) {
        return !(value instanceof t);
      })) {
        var newValue;

        if (Array.isArray(value)) {
          newValue = value.map(function (element) {
            return _clone(element);
          });
        } else {
          if (visitedObjects.indexOf(value) > -1) {
            warn("A circular reference was detected and removed from the message.");
            return null;
          }

          visitedObjects.push(value);
          newValue = {};

          for (var name in value) {
            if (value.hasOwnProperty(name)) {
              var clonedValue = _clone(value[name]);

              if (clonedValue !== null) {
                newValue[name] = clonedValue;
              }
            }
          }

          visitedObjects.pop();
        }

        return newValue;
      }

      return value;
    }

    return _clone(object);
  },
  getOrigin: function getOrigin(url, base) {
    // everything except IE11
    if (typeof URL === 'function') {
      try {
        return new URL(url, base).origin;
      } catch (e) {}
    } // ie11 + safari 10


    var doc = document.implementation.createHTMLDocument('');

    if (base) {
      var baseElement = doc.createElement('base');
      baseElement.href = base;
      doc.head.appendChild(baseElement);
    }

    var anchorElement = doc.createElement('a');
    anchorElement.href = url;
    doc.body.appendChild(anchorElement);
    var origin = anchorElement.protocol + '//' + anchorElement.hostname; //ie11, only include port if referenced in initial URL

    if (url.match(/\/\/[^/]+:[0-9]+\//)) {
      origin += anchorElement.port ? ':' + anchorElement.port : '';
    }

    return origin;
  }
};
var _default = util;
exports.default = _default;
module.exports = exports.default;

},{}],3:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _xdmrpc = _interopRequireDefault(_dereq_("./xdmrpc"));

var _util = _interopRequireDefault(_dereq_("../common/util"));

var _featureFlag = _dereq_("./feature-flag");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Connect = /*#__PURE__*/function () {
  function Connect() {
    _classCallCheck(this, Connect);

    this._xdm = new _xdmrpc.default();
  }
  /**
   * Send a message to iframes matching the targetSpec. This message is added to
   *  a message queue for delivery to ensure the message is received if an iframe
   *  has not yet loaded
   *
   * @param type The name of the event type
   * @param targetSpec The spec to match against extensions when sending this event
   * @param event The event payload
   * @param callback A callback to be executed when the remote iframe calls its callback
   */


  _createClass(Connect, [{
    key: "dispatch",
    value: function dispatch(type, targetSpec, event, callback) {
      this._xdm.queueEvent(type, targetSpec, event, callback);

      return this.getExtensions(targetSpec);
    }
    /**
     * Send a message to iframes matching the targetSpec immediately. This message will
     *  only be sent to iframes that are already open, and will not be delivered if none
     *  are currently open.
     *
     * @param type The name of the event type
     * @param targetSpec The spec to match against extensions when sending this event
     * @param event The event payload
     */

  }, {
    key: "broadcast",
    value: function broadcast(type, targetSpec, event) {
      this._xdm.dispatch(type, targetSpec, event, null, null);

      return this.getExtensions(targetSpec);
    }
  }, {
    key: "_createId",
    value: function _createId(extension) {
      if (!extension.addon_key || !extension.key) {
        throw Error('Extensions require addon_key and key');
      }

      return extension.addon_key + '__' + extension.key + '__' + _util.default.randomString();
    }
    /**
    * Creates a new iframed module, without actually creating the DOM element.
    * The iframe attributes are passed to the 'setupCallback', which is responsible for creating
    * the DOM element and returning the window reference.
    *
    * @param extension The extension definition. Example:
    *   {
    *     addon_key: 'my-addon',
    *     key: 'my-module',
    *     url: 'https://example.com/my-module',
    *     options: {
    *         autoresize: false,
    *         hostOrigin: 'https://connect-host.example.com/'
    *     }
    *   }
    *
    * @param initCallback The optional initCallback is called when the bridge between host and iframe is established.
    **/

  }, {
    key: "create",
    value: function create(extension, initCallback, unloadCallback) {
      var extension_id = this.registerExtension(extension, initCallback, unloadCallback);
      var options = extension.options || {};
      options.platformFeatureFlags = (0, _featureFlag.getPlatformFeatureFlags)();
      var data = {
        extension_id: extension_id,
        api: this._xdm.getApiSpec(extension.addon_key),
        origin: _util.default.locationOrigin(),
        options: options
      };
      return {
        id: extension_id,
        name: JSON.stringify(data),
        src: extension.url
      };
    } // This is called from ACJS
    // noinspection JSUnusedGlobalSymbols

  }, {
    key: "registerRequestNotifier",
    value: function registerRequestNotifier(callback) {
      this._xdm.registerRequestNotifier(callback);
    }
  }, {
    key: "registerExtension",
    value: function registerExtension(extension, initCallback, unloadCallback) {
      var extension_id = this._createId(extension);

      this._xdm.registerExtension(extension_id, {
        extension: extension,
        initCallback: initCallback,
        unloadCallback: unloadCallback
      });

      return extension_id;
    }
  }, {
    key: "registerKeyListener",
    value: function registerKeyListener(extension_id, key, modifiers, callback) {
      this._xdm.registerKeyListener(extension_id, key, modifiers, callback);
    }
  }, {
    key: "unregisterKeyListener",
    value: function unregisterKeyListener(extension_id, key, modifiers, callback) {
      this._xdm.unregisterKeyListener(extension_id, key, modifiers, callback);
    }
  }, {
    key: "registerClickHandler",
    value: function registerClickHandler(callback) {
      this._xdm.registerClickHandler(callback);
    }
  }, {
    key: "unregisterClickHandler",
    value: function unregisterClickHandler() {
      this._xdm.unregisterClickHandler();
    }
  }, {
    key: "defineModule",
    value: function defineModule(moduleName, module, options) {
      this._xdm.defineAPIModule(module, moduleName, options);
    }
  }, {
    key: "isModuleDefined",
    value: function isModuleDefined(moduleName) {
      return this._xdm.isAPIModuleDefined(moduleName);
    }
  }, {
    key: "defineGlobals",
    value: function defineGlobals(module) {
      this._xdm.defineAPIModule(module);
    }
  }, {
    key: "getExtensions",
    value: function getExtensions(filter) {
      return this._xdm.getRegisteredExtensions(filter);
    }
  }, {
    key: "unregisterExtension",
    value: function unregisterExtension(filter) {
      return this._xdm.unregisterExtension(filter);
    }
  }, {
    key: "returnsPromise",
    value: function returnsPromise(wrappedMethod) {
      wrappedMethod.returnsPromise = true;
    }
  }, {
    key: "setFeatureFlagGetter",
    value: function setFeatureFlagGetter(getBooleanFeatureFlag) {
      this._xdm.setFeatureFlagGetter(getBooleanFeatureFlag);
    }
  }, {
    key: "registerExistingExtension",
    value: function registerExistingExtension(extension_id, data) {
      return this._xdm.registerExtension(extension_id, data);
    }
  }]);

  return Connect;
}();

var _default = Connect;
exports.default = _default;
module.exports = exports.default;

},{"../common/util":2,"./feature-flag":4,"./xdmrpc":6}],4:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPlatformFeatureFlags = getPlatformFeatureFlags;
var allowedPlatformFeatureFlags = ['platform-visual-refresh-icons'];

function getPlatformFeatureFlags() {
  var flags = {};

  if (window.connectHost && window.connectHost.getBooleanFeatureFlag) {
    allowedPlatformFeatureFlags.forEach(function (key) {
      return flags[key] = window.connectHost.getBooleanFeatureFlag(key);
    });
  }

  return flags;
}

},{}],5:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _connect = _interopRequireDefault(_dereq_("./connect"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = new _connect.default();

exports.default = _default;
module.exports = exports.default;

},{"./connect":3}],6:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _util = _interopRequireDefault(_dereq_("../common/util"));

var _postmessage = _interopRequireDefault(_dereq_("../common/postmessage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var VALID_EVENT_TIME_MS = 30000; //30 seconds

var XDMRPC = /*#__PURE__*/function (_PostMessage) {
  _inherits(XDMRPC, _PostMessage);

  var _super = _createSuper(XDMRPC);

  function XDMRPC(config) {
    var _this;

    _classCallCheck(this, XDMRPC);

    config = config || {};
    _this = _super.call(this, config);
    _this._registeredExtensions = config.extensions || {};
    _this._registeredAPIModules = {};
    _this._registeredAPIModules._globals = {};
    _this._pendingCallbacks = {};
    _this._keycodeCallbacks = {};
    _this._clickHandlers = [];
    _this._pendingEvents = {};
    _this._messageHandlers = {
      init: _this._handleInit,
      req: _this._handleRequest,
      resp: _this._handleResponse,
      broadcast: _this._handleBroadcast,
      event_query: _this._handleEventQuery,
      key_triggered: _this._handleKeyTriggered,
      addon_clicked: _this._handleAddonClick,
      get_host_offset: _this._getHostOffset,
      unload: _this._handleUnload
    };
    return _this;
  }

  _createClass(XDMRPC, [{
    key: "_padUndefinedArguments",
    value: function _padUndefinedArguments(array, length) {
      return array.length >= length ? array : array.concat(new Array(length - array.length));
    }
  }, {
    key: "_verifyAPI",
    value: function _verifyAPI(event, reg) {
      var untrustedTargets = event.data.targets;

      if (!untrustedTargets) {
        return;
      }

      var trustedSpec = this.getApiSpec();
      var tampered = false;

      function check(trusted, untrusted) {
        Object.getOwnPropertyNames(untrusted).forEach(function (name) {
          if (_typeof(untrusted[name]) === 'object' && trusted[name]) {
            check(trusted[name], untrusted[name]);
          } else {
            if (untrusted[name] === 'parent' && trusted[name]) {
              tampered = true;
            }
          }
        });
      }

      check(trustedSpec, untrustedTargets);

      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          type: 'api_tamper',
          tampered: tampered
        }, reg.extension.url);
      } else {
        console.warn("_verifyAPI postMessage skipped as event source missing.");
      }
    }
  }, {
    key: "_handleInit",
    value: function _handleInit(event, reg) {
      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          type: 'init_received'
        }, reg.extension.url);
      } else {
        console.warn("_handleInit postMessage skipped as event source missing.");
      }

      this._registeredExtensions[reg.extension_id].source = event.source;

      if (reg.initCallback) {
        reg.initCallback(event.data.eid);
        delete reg.initCallback;
      }

      if (event.data.targets) {
        this._verifyAPI(event, reg);
      }
    }
  }, {
    key: "_getHostOffset",
    value: function _getHostOffset(event, _window) {
      var hostWindow = event.source;
      var hostFrameOffset = null;
      var windowReference = _window || window; // For testing

      if (windowReference === windowReference.top && typeof windowReference.getHostOffsetFunctionOverride === 'function') {
        hostFrameOffset = windowReference.getHostOffsetFunctionOverride(hostWindow);
      }

      if (typeof hostFrameOffset !== 'number') {
        hostFrameOffset = 0; // Find the closest frame that has the same origin as event source

        while (!this._hasSameOrigin(hostWindow)) {
          // Climb up the iframe tree 1 layer
          hostFrameOffset++;
          hostWindow = hostWindow.parent;
        }
      }

      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          hostFrameOffset: hostFrameOffset
        }, event.origin);
      } else {
        console.warn("_getHostOffset postMessage skipped as event source missing.");
      }
    }
  }, {
    key: "_hasSameOrigin",
    value: function _hasSameOrigin(window) {
      if (window === window.top) {
        return true;
      }

      try {
        // Try set & read a variable on the given window
        // If we can successfully read the value then it means the given window has the same origin
        // as the window that is currently executing the script
        var testVariableName = 'test_var_' + Math.random().toString(16).substr(2);
        window[testVariableName] = true;
        return window[testVariableName];
      } catch (e) {// A exception will be thrown if the windows doesn't have the same origin
      }

      return false;
    }
  }, {
    key: "_handleResponse",
    value: function _handleResponse(event) {
      var data = event.data;
      var pendingCallback = this._pendingCallbacks[data.mid];

      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];
        pendingCallback.apply(window, data.args);
      }
    }
  }, {
    key: "registerRequestNotifier",
    value: function registerRequestNotifier(cb) {
      this._registeredRequestNotifier = cb;
    }
  }, {
    key: "_handleRequest",
    value: function _handleRequest(event, reg) {
      function sendResponse() {
        var args = _util.default.sanitizeStructuredClone(_util.default.argumentsToArray(arguments));

        if (event.source && event.source.postMessage) {
          // only post a message if the source of the event still exists
          event.source.postMessage({
            mid: event.data.mid,
            type: 'resp',
            forPlugin: true,
            args: args
          }, reg.extension.url);
        } else {
          console.warn("_handleRequest postMessage skipped as event source missing.");
        }
      }

      var data = event.data;
      var module = this._registeredAPIModules[data.mod];
      var extension = this.getRegisteredExtensions(reg.extension)[0];

      if (module) {
        var fnName = data.fn;

        if (data._cls) {
          var Cls = module[data._cls];
          var ns = data.mod + '-' + data._cls + '-';
          sendResponse._id = data._id;

          if (fnName === 'constructor') {
            if (!Cls._construct) {
              Cls.constructor.prototype._destroy = function () {
                delete this._context._proxies[ns + this._id];
              };

              Cls._construct = function () {
                for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
                  args[_key] = arguments[_key];
                }

                var inst = _construct(Cls.constructor, args);

                var callback = args[args.length - 1];
                inst._id = callback._id;
                inst._context = callback._context;
                inst._context._proxies[ns + inst._id] = inst;
                return inst;
              };
            }

            module = Cls;
            fnName = '_construct';
          } else {
            module = extension._proxies[ns + data._id];
          }
        }

        var method = module[fnName];

        if (method) {
          var methodArgs = data.args;
          var padLength = method.length - 1;

          if (fnName === '_construct') {
            padLength = module.constructor.length - 1;
          }

          sendResponse._context = extension;
          methodArgs = this._padUndefinedArguments(methodArgs, padLength);
          methodArgs.push(sendResponse);
          var promiseResult = method.apply(module, methodArgs);

          if (method.returnsPromise) {
            if (!(_typeof(promiseResult) === 'object' || typeof promiseResult === 'function') || typeof promiseResult.then !== 'function') {
              sendResponse('Defined module method did not return a promise.');
            } else {
              promiseResult.then(function (result) {
                sendResponse(undefined, result);
              }).catch(function (err) {
                err = err instanceof Error ? err.message : err;
                sendResponse(err);
              });
            }
          }

          if (this._registeredRequestNotifier) {
            this._registeredRequestNotifier.call(null, {
              module: data.mod,
              fn: data.fn,
              type: data.type,
              args: methodArgs,
              addon_key: reg.extension.addon_key,
              key: reg.extension.key,
              extension_id: reg.extension_id
            });
          }
        }
      }
    }
  }, {
    key: "_handleBroadcast",
    value: function _handleBroadcast(event, reg) {
      var event_data = event.data;

      var targetSpec = function targetSpec(r) {
        return r.extension.addon_key === reg.extension.addon_key && r.extension_id !== reg.extension_id;
      };

      this.dispatch(event_data.etyp, targetSpec, event_data.evnt, null, null);
    }
  }, {
    key: "_handleKeyTriggered",
    value: function _handleKeyTriggered(event, reg) {
      var eventData = event.data;

      var keycodeEntry = this._keycodeKey(eventData.keycode, eventData.modifiers, reg.extension_id);

      var listeners = this._keycodeCallbacks[keycodeEntry];

      if (listeners) {
        listeners.forEach(function (listener) {
          listener.call(null, {
            addon_key: reg.extension.addon_key,
            key: reg.extension.key,
            extension_id: reg.extension_id,
            keycode: eventData.keycode,
            modifiers: eventData.modifiers
          });
        }, this);
      }
    }
  }, {
    key: "defineAPIModule",
    value: function defineAPIModule(module, moduleName) {
      moduleName = moduleName || '_globals';
      this._registeredAPIModules[moduleName] = _util.default.extend({}, this._registeredAPIModules[moduleName] || {}, module);
      return this._registeredAPIModules;
    }
  }, {
    key: "isAPIModuleDefined",
    value: function isAPIModuleDefined(moduleName) {
      return typeof this._registeredAPIModules[moduleName] !== 'undefined';
    }
  }, {
    key: "_pendingEventKey",
    value: function _pendingEventKey(targetSpec, time) {
      var key = targetSpec.addon_key || 'global';

      if (targetSpec.key) {
        key = "".concat(key, "@@").concat(targetSpec.key);
      }

      key = "".concat(key, "@@").concat(time);
      return key;
    }
  }, {
    key: "queueEvent",
    value: function queueEvent(type, targetSpec, event, callback) {
      var loaded_frame,
          targets = this._findRegistrations(targetSpec);

      loaded_frame = targets.some(function (target) {
        return target.registered_events !== undefined;
      }, this);

      if (loaded_frame) {
        this.dispatch(type, targetSpec, event, callback);
      } else {
        this._cleanupInvalidEvents();

        var time = new Date().getTime();
        this._pendingEvents[this._pendingEventKey(targetSpec, time)] = {
          type: type,
          targetSpec: targetSpec,
          event: event,
          callback: callback,
          time: time,
          uid: _util.default.randomString()
        };
      }
    }
  }, {
    key: "_cleanupInvalidEvents",
    value: function _cleanupInvalidEvents() {
      var _this2 = this;

      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this2._pendingEvents[index];
        var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;

        if (!eventIsValid) {
          delete _this2._pendingEvents[index];
        }
      });
    }
  }, {
    key: "_handleEventQuery",
    value: function _handleEventQuery(message, extension) {
      var _this3 = this;

      var executed = {};
      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this3._pendingEvents[index];
        var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;
        var isSameTarget = !element.targetSpec || _this3._findRegistrations(element.targetSpec).length !== 0;

        if (isSameTarget && element.targetSpec.key) {
          isSameTarget = element.targetSpec.addon_key === extension.extension.addon_key && element.targetSpec.key === extension.extension.key;
        }

        if (eventIsValid && isSameTarget) {
          executed[index] = element;
          element.targetSpec = element.targetSpec || {};

          _this3.dispatch(element.type, element.targetSpec, element.event, element.callback, message.source);
        } else if (!eventIsValid) {
          delete _this3._pendingEvents[index];
        }
      });
      this._registeredExtensions[extension.extension_id].registered_events = message.data.args;
      return executed;
    }
  }, {
    key: "_handleUnload",
    value: function _handleUnload(event, reg) {
      if (!reg) {
        return;
      }

      if (reg.extension_id && this._registeredExtensions[reg.extension_id]) {
        delete this._registeredExtensions[reg.extension_id].source;
      }

      if (reg.unloadCallback) {
        reg.unloadCallback(event.data.eid);
      }
    }
  }, {
    key: "dispatch",
    value: function dispatch(type, targetSpec, event, callback, source) {
      function sendEvent(reg, evnt) {
        if (reg.source && reg.source.postMessage) {
          var mid;

          if (callback) {
            mid = _util.default.randomString();
            this._pendingCallbacks[mid] = callback;
          }

          reg.source.postMessage({
            type: 'evt',
            mid: mid,
            etyp: type,
            evnt: evnt
          }, reg.extension.url);
        }
      }

      var registrations = this._findRegistrations(targetSpec || {});

      registrations.forEach(function (reg) {
        if (source && !reg.source) {
          reg.source = source;
        }

        if (reg.source) {
          _util.default._bind(this, sendEvent)(reg, event);
        }
      }, this);
    }
  }, {
    key: "_findRegistrations",
    value: function _findRegistrations(targetSpec) {
      var _this4 = this;

      if (this._registeredExtensions.length === 0) {
        _util.default.error('no registered extensions', this._registeredExtensions);

        return [];
      }

      var keys = Object.getOwnPropertyNames(targetSpec);
      var registrations = Object.getOwnPropertyNames(this._registeredExtensions).map(function (key) {
        return _this4._registeredExtensions[key];
      });

      if (targetSpec instanceof Function) {
        return registrations.filter(targetSpec);
      } else {
        return registrations.filter(function (reg) {
          return keys.every(function (key) {
            return reg.extension[key] === targetSpec[key];
          });
        });
      }
    }
  }, {
    key: "registerExtension",
    value: function registerExtension(extension_id, data) {
      data._proxies = {};
      data.extension_id = extension_id;
      this._registeredExtensions[extension_id] = data;
    }
  }, {
    key: "_keycodeKey",
    value: function _keycodeKey(key, modifiers, extension_id) {
      var code = key;

      if (modifiers) {
        if (typeof modifiers === "string") {
          modifiers = [modifiers];
        }

        modifiers.sort();
        modifiers.forEach(function (modifier) {
          code += '$$' + modifier;
        }, this);
      }

      return code + '__' + extension_id;
    }
  }, {
    key: "registerKeyListener",
    value: function registerKeyListener(extension_id, key, modifiers, callback) {
      if (typeof modifiers === "string") {
        modifiers = [modifiers];
      }

      var reg = this._registeredExtensions[extension_id];

      var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);

      if (!this._keycodeCallbacks[keycodeEntry]) {
        this._keycodeCallbacks[keycodeEntry] = [];
        reg.source.postMessage({
          type: 'key_listen',
          keycode: key,
          modifiers: modifiers,
          action: 'add'
        }, reg.extension.url);
      }

      this._keycodeCallbacks[keycodeEntry].push(callback);
    }
  }, {
    key: "unregisterKeyListener",
    value: function unregisterKeyListener(extension_id, key, modifiers, callback) {
      var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);

      var potentialCallbacks = this._keycodeCallbacks[keycodeEntry];
      var reg = this._registeredExtensions[extension_id];

      if (potentialCallbacks) {
        if (callback) {
          var index = potentialCallbacks.indexOf(callback);

          this._keycodeCallbacks[keycodeEntry].splice(index, 1);
        } else {
          delete this._keycodeCallbacks[keycodeEntry];
        }

        if (reg.source && reg.source.postMessage) {
          reg.source.postMessage({
            type: 'key_listen',
            keycode: key,
            modifiers: modifiers,
            action: 'remove'
          }, reg.extension.url);
        }
      }
    }
  }, {
    key: "registerClickHandler",
    value: function registerClickHandler(callback) {
      if (typeof callback !== 'function') {
        throw new Error('callback must be a function');
      }

      this._clickHandlers.push(callback);
    }
  }, {
    key: "_handleAddonClick",
    value: function _handleAddonClick(event, reg) {
      for (var i = 0; i < this._clickHandlers.length; i++) {
        if (typeof this._clickHandlers[i] === 'function') {
          this._clickHandlers[i]({
            addon_key: reg.extension.addon_key,
            key: reg.extension.key,
            extension_id: reg.extension_id
          });
        }
      }
    }
  }, {
    key: "unregisterClickHandler",
    value: function unregisterClickHandler() {
      this._clickHandlers = [];
    }
  }, {
    key: "getApiSpec",
    value: function getApiSpec(addonKey) {
      var _this5 = this;

      function getModuleDefinition(mod) {
        return Object.getOwnPropertyNames(mod).reduce(function (accumulator, memberName) {
          var member = mod[memberName];

          switch (_typeof(member)) {
            case 'function':
              accumulator[memberName] = {
                args: _util.default.argumentNames(member),
                returnsPromise: member.returnsPromise || false
              };
              break;

            case 'object':
              if (member.hasOwnProperty('constructor')) {
                accumulator[memberName] = getModuleDefinition(member);
              }

              break;
          }

          return accumulator;
        }, {});
      }

      return Object.getOwnPropertyNames(this._registeredAPIModules).reduce(function (accumulator, moduleName) {
        var module = _this5._registeredAPIModules[moduleName];

        if (typeof module.addonKey === 'undefined' || module.addonKey === addonKey) {
          accumulator[moduleName] = getModuleDefinition(module);
        }

        return accumulator;
      }, {});
    }
  }, {
    key: "_originEqual",
    value: function _originEqual(url, origin) {
      function strCheck(str) {
        return typeof str === 'string' && str.length > 0;
      }

      var urlOrigin = _util.default.getOrigin(url); // check strings are strings and they contain something


      if (!strCheck(url) || !strCheck(origin) || !strCheck(urlOrigin)) {
        return false;
      }

      return origin === urlOrigin;
    } // validate origin of postMessage

  }, {
    key: "_checkOrigin",
    value: function _checkOrigin(event, reg) {
      var no_source_types = ['init'];
      var isNoSourceType = reg && !reg.source && no_source_types.indexOf(event.data.type) > -1;
      var sourceTypeMatches = reg && event.source === reg.source;

      var hasExtensionUrl = reg && this._originEqual(reg.extension.url, event.origin);

      var isValidOrigin = hasExtensionUrl && (isNoSourceType || sourceTypeMatches); // get_host_offset fires before init

      if (event.data.type === 'get_host_offset' && window === window.top) {
        isValidOrigin = true;
      } // check undefined for chromium (Issue 395010)


      if (event.data.type === 'unload' && (sourceTypeMatches || event.source === undefined)) {
        isValidOrigin = true;
      }

      return isValidOrigin;
    }
  }, {
    key: "getRegisteredExtensions",
    value: function getRegisteredExtensions(filter) {
      if (filter) {
        return this._findRegistrations(filter);
      }

      return this._registeredExtensions;
    }
  }, {
    key: "unregisterExtension",
    value: function unregisterExtension(filter) {
      var registrations = this._findRegistrations(filter);

      if (registrations.length !== 0) {
        registrations.forEach(function (registration) {
          var _this6 = this;

          var keys = Object.keys(this._pendingEvents);
          keys.forEach(function (index) {
            var element = _this6._pendingEvents[index];
            var targetSpec = element.targetSpec || {};

            if (targetSpec.addon_key === registration.extension.addon_key && targetSpec.key === registration.extension.key) {
              delete _this6._pendingEvents[index];
            }
          });
          delete this._registeredExtensions[registration.extension_id];
        }, this);
      }
    }
  }, {
    key: "setFeatureFlagGetter",
    value: function setFeatureFlagGetter(getBooleanFeatureFlag) {
      this._getBooleanFeatureFlag = getBooleanFeatureFlag;
    }
  }]);

  return XDMRPC;
}(_postmessage.default);

var _default = XDMRPC;
exports.default = _default;
module.exports = exports.default;

},{"../common/postmessage":1,"../common/util":2}]},{},[5])(5)
});